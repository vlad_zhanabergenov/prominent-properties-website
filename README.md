Technologies in use: [Next.js](https://nextjs.org/), TypeScript, Algolia, GraphQL, Contentful, Cloudinary.

## Getting Started

To run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
