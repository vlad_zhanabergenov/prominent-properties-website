declare type AnyProperty = PropertyToLet | PropertyForSale

declare type AnyPropertyWithCustomFields = (PropertyToLet | PropertyForSale) & {
  min_price?: number
  max_price?: number
  hasSpecialDeals?: boolean
  min_esc?: number
  max_esc?: number
}

declare type Property = Partial<{
  __typename: string
  available_date: string
  available_type: string
  best_image: string
  city: string
  cluster: string
  complex_space: number
  country: string
  general_features: GeneralFeatures
  gmaven_mapped_key: string
  property_gmaven_key: string
  gross_price: number
  industrial_features: IndustrialFeatures
  latitude: number
  longitude: number
  marketing: MarketingType
  max_gla: number
  min_gla: number
  objectID: string
  office_features: OfficeFeatures
  parking: Parking
  property_category: string
  property_featured: number
  property_images: Image[]
  property_name: string
  property_responsibility: Responsibility[]
  province: string
  secondary_category: string
  space_breakdown: ComplexSpaceType[]
  status: string
  status_date: string
  street_address: string
  property_slug: string
  suburb: string
  total_erf_extent: number
  total_property_gla: number
  unit_category: string
  unit_id: string
  video: string
  virtual_tour: string
  web_ref: string
}>

declare type PropertyToLet = Partial<Property & {
  dealType: 'toLet'
  combinable: string
  net_price: number
  escalation: number
  property_sub_category: string
  property_tia: TiaType[]
  property_update_date: string
  property_update_datetime: number
  property_video: string
  property_virtual_tour: string
  special_deals: string
  sub_divisible: string
  this_unit_can_be_leased_by_itself: string
  tia: TiaType[]
  unit_images: Image[]
  unit_responsibility: Responsibility[]
  unit_update_date: string
  unit_update_datetime: number
  unit_slug: string
}>

declare type PropertyForSale = Partial<Property & {
  dealType: 'forSale'
  property_type: string
  update_date: string
  update_datetime: number
}>

declare type GeneralFeatures = Partial<{
  backup_water_supply: string
  boardroom: string
  canteen: string
  green_certification: string
  green_certification_rating: number
  gym: string
  has_generators: string
  has_security: string
  meeting_rooms: string
  property_kitchenette: string
  property_naming_rights: string
  reception: string
  security_guards: number
  security_hours: number
  security_infrustructure: string
  security_responsibility: string
  showers: string
  solar: string
  unit_kitchenette: string
  unit_naming_rights: string
}>

declare type IndustrialFeatures = Partial<{
  floor_load_capacity: number
  gantry_cranes: string
  has_yard: string
  height_to_eaves: number
  lux_description: string
  lux_level: number
  power_output: number
  power_phase: number
  power_unit: string
}>

declare type MarketingType = Partial<{
  property_marketing_description: string
  property_marketing_heading: string
  unit_marketing_description: string
  unit_marketing_heading: string
}>

declare type OfficeFeatures = Partial<{
  building_shape: string
  has_aircon: string
  has_internet: string
  internet_provider: string
  lift_cores: number
  lift_count: number
  no_floors: number
}>

declare type Parking = Partial<{
  property_parking: string
  property_parking_basement: number
  property_parking_covered: number
  property_parking_open: number
  property_parking_ratio: number
  property_parking_shade: number
  property_parking_tandem: number
  unit_parking: string
  unit_parking_basement: number
  unit_parking_covered: number
  unit_parking_open: number
  unit_parking_ratio: number
  unit_parking_shade: number
  unit_parking_tandem: number
}>

declare type Image = Partial<{
  image_id: string
  image_path_url: string
  type: string
}>

declare type Responsibility = Partial<{
  cell_number: string
  email: string
  facebook_profile: string
  gmaven_contact_key: string
  image: string
  job_title: string
  linkedin_profile: string
  name: string
  role: string
  telephone_number: string
  twitter_profile: string
}>

declare type ComplexSpaceType = Partial<{
  space_asking_gross_monthly_rental_m2: number
  space_asking_net_monthly_rental_m2: number
  space_category: string
  space_gla: number
  space_id: string
}>

declare type TiaType = Partial<{
  tia_period: number
  tia_period_not_specified: number
  tia_type: string
  tia_value: number
  tia_value_based_on: string
}>
