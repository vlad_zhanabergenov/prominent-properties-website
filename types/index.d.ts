declare type NavItem = {
  id: string
  name: string
  path: string
  submenu?: Omit<NavItem, 'submenu'>[]
}

declare type HeroSearchOptions = {
  dealType: ["For Sale", "To Let"]
}

declare type HeroCarouselItem = {
  id: string
  title: string
  image: string
  mobileImage: string
}

declare type PersonItem = {
  id: string
  name: string
  image: string
  title: string
  shortBio: string
  email: string
  phone: string
  linkedIn: string
  twitter: string
}

declare type FeaturedOfferingItem = {
  id: string
  label: string
  image: string
  imageLabel: string
  name: string
  gla: number
  type: 'Commercial'
  location: string
}

declare type NewsMediaItem = {
  id: string
  type: 'news'
  featuredImage: string
  heroImage: string
} & Omit<NewsMediaItemContentful, 'featuredImage' | 'heroImage'>

declare type NewsMediaItemWithTwitter = NewsMediaItem | { id: string, type: 'twitter' }

declare type PDFItem = 'propertyToLet' | 'propertyUnitToLet' | 'propertyForSale'
