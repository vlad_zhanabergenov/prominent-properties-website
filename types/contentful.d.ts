declare type ContentfulFile = {
  metadata: any
  fields: {
    file: {
      url: string
    }
  }
}

declare type HeroCarouselItemContentful = {
  sliderOrder: number
  sliderHasLink: boolean
  title: string
  sliderImage: string
  mobileImage: string
}

declare type PersonItemContentful = {
  name: string
  slug: string
  image: {
    fields: {
      file: {
        url: string
      }
    }
  }
  featuredImage: string
  title: string
  shortBio: string
  bio: string
  email: string
  phone: string
  linkedIn: string
  instagram: string
  facebook: string
  twitter: string
  order: number
}

declare type NewsMediaItemContentful = {
  category: string
  author: string
  publishDate: string
  featuredArticle: boolean
  title: string
  featuredImage: string
  heroImage: string
  articleSummary: string
}
