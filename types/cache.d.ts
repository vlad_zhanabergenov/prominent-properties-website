declare interface AppCache {
  app: App
  search: Search
}

interface App {
  algoliaReady: boolean
  algoliaStats: {
    nbHits: {
      ToLet: number
      ForSale: number
    }
    facets: {
      [key: string]: {
        [key: string]: number
      }
    }
  }
}

interface Search {
  query?: string
  dealType: string
  propertyType: string[]
  city: string[]
  suburb: string[]
  minGLA: string
  maxGLA: string
  minPrice: string
  maxPrice: string
  broker: string[]
  page: string
}
