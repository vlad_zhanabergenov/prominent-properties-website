declare module '*.graphql' {
  import { DocumentNode } from 'graphql'
  const Schema: DocumentNode

  export = Schema
}

declare module 'react-twitter-embed'
declare module 'react-netlify-forms'
declare module 'react-image-video-lightbox'
declare module '*.svg'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.png'

declare const google: any
