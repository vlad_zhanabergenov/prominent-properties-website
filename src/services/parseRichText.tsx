import { Document, BLOCKS, MARKS } from '@contentful/rich-text-types'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import cn from 'classnames'

interface Options {
  classForAll?: string
  h1Class?: string
  h2Class?: string
  h3Class?: string
  h4Class?: string
  pClass?: string
}

const parseRichText = (richTextDocument: Document, options?: Options) => {
  const {
    classForAll,
    h1Class,
    h2Class,
    h3Class,
    h4Class,
    pClass
  } = options || {}

  return documentToReactComponents(richTextDocument, {
    renderNode: {
      [BLOCKS.HEADING_1]: (_node: any, children: any) => (
        <h1 className={ cn(classForAll, h1Class) }>{ children }</h1>
      ),
      [BLOCKS.HEADING_2]: (_node: any, children: any) => (
        <h2 className={ cn(classForAll, h2Class) }>{ children }</h2>
      ),
      [BLOCKS.HEADING_3]: (_node: any, children: any) => (
        <h3 className={ cn(classForAll, h3Class) }>{ children }</h3>
      ),
      [BLOCKS.HEADING_4]: (_node: any, children: any) => (
        <h4 className={ cn(classForAll, h4Class) }>{ children }</h4>
      ),
      [BLOCKS.HEADING_5]: (_node: any, children: any) => (
        <h4 className={ cn(classForAll, h4Class) }>{ children }</h4>
      ),
      [BLOCKS.HEADING_6]: (_node: any, children: any) => (
        <h4 className={ cn(classForAll, h4Class) }>{ children }</h4>
      ),
      [BLOCKS.PARAGRAPH]: (_node: any, children: any) => (
        <p className={ cn(classForAll, pClass) }>{ children }</p>
      )
    },
    renderMark: {
      [MARKS.BOLD]: (text: any) => <b>{ text }</b>,
      [MARKS.ITALIC]: (text: any) => <i>{ text }</i>,
      [MARKS.UNDERLINE]: (text: any) => <u>{ text }</u>
    }
  })
}

export default parseRichText
