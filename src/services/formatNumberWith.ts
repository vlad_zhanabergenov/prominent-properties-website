export default function formatNumberWith(separator: string, num: number) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
}
