export default function addDealTypeField(list: AnyProperty[]): AnyProperty[] {
  return list.map(i => {
    let dealType: AnyProperty['dealType']

    if (i.__typename === 'PropertyForSale') {
      dealType = "forSale"
    } else if (i.__typename === 'PropertyToLet') {
      dealType = "toLet"
    } else {
      dealType = i.dealType || "forSale"
    }

    return { ...i, dealType }
  })
}
