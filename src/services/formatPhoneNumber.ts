import parsePhoneNumber from 'libphonenumber-js'

export default function formatPhoneNumber(string: string) {
  return parsePhoneNumber(string)?.formatInternational() || string
}
