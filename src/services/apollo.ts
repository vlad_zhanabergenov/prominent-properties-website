import { ApolloClient } from '@apollo/client'
import cache from 'cache'

const apolloClient = new ApolloClient({
  uri: process.env.NEXT_PUBLIC_GRAPHQL_URL || "",
  cache,
  ssrMode: true
})

export default apolloClient
