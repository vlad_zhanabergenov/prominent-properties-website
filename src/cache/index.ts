import { InMemoryCache } from "@apollo/client"
import {
  appVar,
  searchVar
} from './vars'

const cache: InMemoryCache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        app: {
          read() { return appVar() }
        },
        search: {
          read() { return searchVar() }
        }
      }
    }
  }
})

export default cache
