import { searchVar } from 'cache/vars'
import initialState from 'cache/initialState'

export default function setField(name: keyof AppCache['search'], value: any) {
  const prev = name === 'dealType' ? initialState.search : searchVar()

  if (name === 'page') {
    searchVar({
      ...prev, [name]: value
    })
  } else {
    searchVar({
      ...prev,
      [name]: value,
      page: '1',
    })
  }
}
