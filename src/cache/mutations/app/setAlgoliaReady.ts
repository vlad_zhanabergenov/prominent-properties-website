import { appVar } from 'cache/vars'

export default function setAlgoliaReady(value: boolean) {
  const prev = appVar()

  appVar({
    ...prev,
    algoliaReady: value
  })
}
