import React, { useEffect } from 'react'
import { useReactiveVar } from '@apollo/client'
import { useRouter } from 'next/router'
import { useForm } from 'hooks'
import searchStateParser from 'services/searchStateParser'
import { searchOptions } from 'utilities/constants'
import { searchVar } from 'cache/vars'
import { setField } from 'cache/mutations/search'
import initialState from 'cache/initialState'

import { CategoryFilter } from './components'
import { Button, Select, SuburbFilter } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'query' | 'dealType'>

const Search: React.FC = () => {
  const router = useRouter()

  const searchState = useReactiveVar(searchVar)

  const handleChange = (name: keyof Options, value: any) => {
    setField(name, value)
  }

  const handleSubmit = () => {
    router.push(`/search?${ searchStateParser.encode(searchState) }`)
  }

  useEffect(() => {
    if (router.pathname === '/') {
      (Object.keys(initialState.search) as (keyof Search)[]).forEach((key) => {
        setField(key, initialState.search[key])
      })
    }
  }, [router.pathname])

  const { values, change, submit } = useForm<Options>({
    fields: {
      dealType: { initialValue: searchState.dealType }
    },
    onChange: handleChange,
    handleSubmit
  }, [searchState])

  return (
    <div className={ s.Search }>
      <div className={ s.layout }>
        <div className={ cn(s.form) }>
          <Select
            name="dealType"
            label='Deal type'
            className={ s.col1 }
            options={ searchOptions.dealType }
            value={ values.dealType }
            onChange={ change }
            placeholder="eg: Rent"
          />
          <CategoryFilter className={ s.col1 }/>
          <SuburbFilter isWidget />
          <Button className={ s.submit } onClick={ submit }>Search</Button>
        </div>
        <div className={ cn(s.line, 'no-mobile') }/>
      </div>
    </div>
  )
}

export default Search
