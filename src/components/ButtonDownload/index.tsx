import { Dropbox } from 'dropbox'
import React, { useState } from 'react'

import s from './style.module.sass'
import cn from 'classnames'



const ButtonDownload: React.FC = () => {
  const [loading, setLoading] = useState(false)

  const DownloadPDF = () => {
    setLoading(true)
    const dbx = new Dropbox({ accessToken: process.env.NEXT_PUBLIC_DROPBOX_ACCESS_TOKEN })
    dbx.sharingGetSharedLinkFile({url: process.env.NEXT_PUBLIC_DROPBOX_URL || '' })
    .then((res: any) => {
      const url = window.URL.createObjectURL(new Blob([res.result.fileBlob]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', `${res.result.name}.pdf`)
      document.body.appendChild(link)
      link.click()
      setLoading(false)
    })
    .catch(() => setLoading(false))
  }


  return (
    <div
      className={ cn(s.ButtonDownload,  "cursor-pointer no-select", {
        "no-pointer-events": loading,
      }) }
      onClick={ DownloadPDF  }
    >
      { !loading ?
          <div>
            <p className='text-center darkCyan bold m-0'>DOWNLOAD</p>
            <p className='text-center white bold m-0'>COMPANY PROFILE</p>
          </div>
        :
          <p className='darkCyan bold'>LOADING...</p> }
    </div>
  )
}

export default ButtonDownload
