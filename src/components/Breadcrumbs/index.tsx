import React from 'react'

import { Link } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

enum Crumbs {
  home,
  deal,
  category,
  city,
  suburb,
  property,
  unit
}

type Element = {
  id: string
  name: string
  link?: string
}

interface Props {
  items: Array<string | { name: string, link: string }>
  className?: string
  property?: AnyProperty
}

const crumbToLink = (index: number, item: string, prevLink?: string) => {
  if (item === 'Search') return ''

  switch (index + 1) {
    case Crumbs.deal:
      return `/search?deal=${item.replace( /\s/g, ' ')}`
    case Crumbs.category:
      return `${prevLink}&category=${item}`
    case Crumbs.city:
      return `${prevLink}&city=${item}`
    case Crumbs.suburb:
      return `${prevLink}&suburb=${item}`
    default:
      return ''
  }
}

const Breadcrumbs: React.FC<Props> = ({ items, className, property }) => {
  const elements: Element[] = [
    { id: '0', name: "Home", link: '/' }
  ]

  items.forEach((item, index) => {
    if (typeof item === 'object') {
      elements.push({ id: `${ elements.length }`, name: item.name, link: item.link })
    } else {
      elements.push({ id: `${ elements.length }`, name: item, link: crumbToLink(index, item, elements[index]?.link) })
    }
  })

  return (
    <div className={ cn(s.Breadcrumbs, className || "") }>
      <div className={ cn(s.list) }>
        { elements.map((item, num) =>
          num !== 3 &&
          <React.Fragment key={ item.id }>
            { item.link
              ? !(num === Crumbs.deal && (Number(item.id) === elements.length - 1))
                ? <Link to={ item.link } className={ cn(s.element, 'small light cyan-3 uppercase') }>{ item.name }</Link>
                : <p className={ cn(s.element, 'small bold uppercase darkGrey') }>{ item.name }</p>
              : num === Crumbs.property && (Number(item.id) < elements.length - 1)
                ? <Link to='property' data={{ property: property! }} className={ cn(s.element, 'small light darkGrey uppercase') }>{ item.name }</Link>
                : <p className={ cn(s.element, 'small bold darkGrey uppercase') }>{ item.name }</p>
            }
            { num + 1 < elements.length && <p className='openSans m-0 darkGrey mh-1 no-pointer-events bold'>{'>'}</p> }
          </React.Fragment>
        )}
      </div>
    </div>
  )
}

export default Breadcrumbs
