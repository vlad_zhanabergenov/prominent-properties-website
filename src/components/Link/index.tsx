import NextLink from 'next/link'
import React from 'react'

import cn from 'classnames'
import slugParser from 'services/slugParser'

interface Props {
  to: string
  data?: {
    isUnit?: boolean
    property: AnyProperty
  }
  className?: string
  transparent?: boolean
  scroll?: boolean
  newTab?: boolean
  disabled?: boolean
  onClick?: () => void
}

const Link: React.FC<Props> = ({
  to,
  data,
  className,
  transparent,
  scroll = true,
  newTab,
  disabled,
  onClick,
  children
}) => {
  if (transparent) {
    return <>{ children }</>
  }

  let href: string
  let as: string | undefined

  if (to === 'property' && data) {
    const item = data.property
    let isUnit: boolean

    if (item.property_slug) {
      let path = slugParser.parse(item.property_slug)

      if (typeof data.isUnit === 'boolean') {
        isUnit = data.isUnit
      } else {
        if (item.dealType === 'toLet') {
          isUnit = false
        } else {
          isUnit = 'property_type' in item && item.property_type === 'property_unit'
        }
      }

      if (isUnit) {
        if (item.dealType === 'toLet') {
          path = slugParser.parse(item.unit_slug!)
        }
      }

      href = '/[...property]'
      as = slugParser.stringify(path).slice(0, -1)
    } else {
      href = '/'
    }
  } else {
    href = as = to
  }

  if (newTab) {
    return (
      <a href={ href } target="_blank" className={ cn(className, { 'no-pointer-events': disabled }) } onClick={ onClick }>
        { children }
      </a>
    )
  }

  return (
    <NextLink href={ href } as={ as } scroll={ scroll }>
      <a className={ cn(className, { 'no-pointer-events': disabled }) } onClick={ onClick }>{ children }</a>
    </NextLink>
  )
}

export default Link
