import React, { useEffect } from 'react'
import { useReactiveVar } from '@apollo/client'
import { searchVar } from 'cache/vars'
import { setAlgoliaReady, setAlgoliaStatsField } from 'cache/mutations/app'
import { isEmpty, chain } from 'lodash'
import { Configure, connectSearchBox, connectStateResults, Index, SearchBoxProvided, StateResultsProvided } from 'react-instantsearch-core'
import { InstantSearch } from 'react-instantsearch-dom'
import searchClient from 'services/algolia'
import { searchIndexes, searchOptions, searchConfig } from 'utilities/constants'

interface Props {
  propertiesCounts: {
    [searchIndexes.Rent]: number
    [searchIndexes.Buy]: number
  }
}

const SearchContextRaw: React.FC<Props & StateResultsProvided & SearchBoxProvided> = ({
  propertiesCounts,
  searchState, searchResults,
  currentRefinement, refine,
  children
}) => {
  const { query } = useReactiveVar(searchVar)

  useEffect(() => {
    if (!isEmpty(searchState) && searchResults) {
      setAlgoliaReady(true)
    }
  }, [searchState, searchResults])

  useEffect(() => {
    if (searchResults) {
      const convertedFacets = chain(searchResults.facets).keyBy('name').mapValues('data').value()
      setAlgoliaStatsField('facets', convertedFacets)
    }
  }, [searchResults])

  useEffect(() => {
    setAlgoliaStatsField('nbHits', {
      [searchIndexes.Rent]: propertiesCounts[searchIndexes.Rent],
      [searchIndexes.Buy]: propertiesCounts[searchIndexes.Buy]
    })
  }, [propertiesCounts])

  useEffect(() => {
    if (!currentRefinement || query !== currentRefinement) {
      refine(query)
    }
  }, [query])

  return (
    <>{ children }</>
  )
}

const SearchContextConnected = connectSearchBox(connectStateResults(SearchContextRaw))

const SearchContext: React.FC<Props> = ({ propertiesCounts, children }) => {
  const { query, dealType } = useReactiveVar(searchVar)
  const indexName = dealType === searchOptions.dealType[0] ? searchIndexes.Buy : searchIndexes.Rent

  return (
    <InstantSearch searchClient={ searchClient } indexName={ searchIndexes.Rent }>
      <SearchContextConnected propertiesCounts={ propertiesCounts } defaultRefinement={ query }>
        <Configure { ...searchConfig }/>
        <Index indexName={ searchIndexes.Buy }/>
        <Index indexName={ indexName }>
          { children }
        </Index>
      </SearchContextConnected>
    </InstantSearch>
  )
}

export default SearchContext
