import React from 'react'

import { Button } from 'components'

import s from './style.module.sass'
import LinkedInIcon from 'assets/icons/linkedin.svg'
import TwitterIcon from 'assets/icons/twitter.svg'

const SocialLinks: React.FC = () => {
  return (
    <div className={ s.SocialLinks }>
      <Button secondaryHover transparent icon={ LinkedInIcon } iconSize='xl'/>
      <Button secondaryHover transparent icon={ TwitterIcon } iconSize='xl' className='ml-05'/>
    </div>
  )
}

export default SocialLinks
