import { Link } from 'components'
import React, { useEffect, useState } from 'react'

import { SocialLinks } from './components'

import cn from 'classnames'
import s from './style.module.sass'
import LogoIcon from 'assets/pictures/fullLogo.svg'
import FooterImage from 'assets/pictures/footer.png'
import FooterMobileImage from 'assets/pictures/footerMobile.png'

const Footer: React.FC = () => {
  const [isMobile, setIsMobile] = useState(false)

  useEffect(() => {
    setIsMobile(window.innerWidth <= 767)
  }, [])

  return (
    <div className={ s.Footer }>
      <div className={ s.top }>
        <div className={ s.info }>
          <h2 className='small cyan-4 bold m-0 no-mobile fontSecondary'>DEVOTED EXPERTISE, <span className='white fontSecondary'>BESPOKE SOLUTIONS</span></h2>
          <h2 className='cyan-4 bold m-0 no-desktop small fontSecondary no-tablet'>DEVOTED EXPERTISE,<br/> <span className='white fontSecondary'>BESPOKE SOLUTIONS</span></h2>
          <p className='white light m-0 no-mobile'>
            Situated in the very busy industrial node of Bloemfontein. This Community centre prides itself
            onproviding future shoppers with best value for their money, convenience and ease of access.
            The developer aims to bring an asian influence in term of retail along with national tenants
          </p>
        </div>
        <div className={ s.image }>
          <img src={ isMobile ? FooterMobileImage : FooterImage } className='covered' alt="Prominent Properties"/>
        </div>
      </div>
      <div className={ s.bottom }>
        <div className={ s.line }/>
        <div className={ s.layer }/>
        <div className={ cn(s.layout, "ph-content box-sizing") }>
          <div className={ cn(s.left, 'box-sizing') }>
            <div className={ s.icon }>
              <img src={ LogoIcon } alt="logo" className="contained"/>
            </div>
          </div>
          <div className={ cn(s.center) }>
            <h4 className="bold white mv-05 mt-0 no-mobile">Physical Address:</h4>
            <h3 className="bold white mv-05 mt-0 no-desktop no-tablet">Physical Address:</h3>
            <div className={ s.wrapper }>
              <div className={ s.left }>
                <h4 className="light white m-0 no-mobile">
                  La Rocca Block C, Ground Floor<br/>
                  321 Main Road<br/>
                  Bryanston 2037
                </h4>
                <h3 className="light white m-0 no-desktop no-tablet">
                  La Rocca Block C, Ground Floor<br/>
                  321 Main Road<br/>
                  Bryanston 2037
                </h3>
              </div>
              <div className={ s.right }>
                <h4 className="light white m-0 mb-05 no-mobile">
                  T +27 (0)11 463 0518<br/>
                  E admin@prominentprops.co.za
                </h4>
                <h3 className="light white m-0 mb-05 no-desktop no-tablet">
                  T +27 (0)11 463 0518<br/>
                  E admin@prominentprops.co.za
                </h3>
                <Link to="/contact"><h4 className="bold white m-0 no-mobile">View map</h4></Link>
                <Link to="/contact"><h3 className="bold white m-0 no-desktop no-tablet">View map</h3></Link>
              </div>
            </div>
          </div>
          <div className={ cn(s.right) }>
            <SocialLinks/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
