import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { navItems } from 'utilities/constants'

import { Icon, Link } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

interface Props {
  onSelect?: () => void
}

const Navigation: React.FC<Props> = ({ onSelect }) => {
  const { pathname } = useRouter()

  const list = navItems.map(i => ({
    ...i,
    active: i.path === "/" ? pathname === "/" : pathname.startsWith(i.path)
  }))

  const [currentSubmenu, setCurrentSubmenu] = useState<string | null>(null)

  const handleClick = () => {
    if (onSelect) {
      onSelect()
    }
  }

  const handleSubmenu = (i: NavItem) => {
    if (!!i.submenu?.length) {
      setCurrentSubmenu(current => {
        return current === i.id ? null : i.id
      })
    }
  }

  return (
    <>
      <div className={ cn(s.Navigation, "no-tablet no-mobile") }>
        { list.map(i =>
          <Link key={ i.id } to={ i.path } onClick={ handleClick }
                className={ cn(s.item, 'no-select', { [`no-pointer-events ${ s.active }`]: i.active }) }>
            <p className={ cn('small light uppercase white m-0', { 'bold': i.active }) }>{ i.name }</p>
            <div className={ s.underscore }/>
          </Link>
        )}
      </div>
      <div className={ cn(s.Navigation, "no-desktop") }>
        { list.map(i =>
          <Link key={ i.id } to={ i.path } transparent={ !!i.submenu } onClick={ handleClick }
                className={ cn('no-select', { [s.active]: i.active }) }>
            <div className={ cn(s.item, "box-sizing no-desktop") } onClick={ () => handleSubmenu(i) }>
              <div className={ s.name }>
                <p className={ cn('big no-select uppercase black light m-0 limit-string-1', { 'greyBlue-3': i.id === currentSubmenu }) }>
                  { i.name }
                </p>
                <Icon src={ ArrowIcon } rotate={ i.id === currentSubmenu ? 180 : 90 }/>
              </div>
              { i.submenu && i.id === currentSubmenu &&
                <div className={ cn(s.submenu, "ph-2 pt-1 box-sizing") }>
                  { i.submenu.map(i => ({ ...i, active: pathname.startsWith(i.path) })).map(i =>
                    <Link key={ i.id } to={ i.path } onClick={ handleClick }
                          className={ cn('no-select', { [s.active]: i.active }) }>
                      <div className={ cn(s.name, "pv-05") }>
                        <p className='no-select white light m-0 limit-string-1'>{ i.name }</p>
                        <Icon src={ ArrowIcon } rotate={ 90 } size='s'/>
                      </div>
                    </Link>
                  )}
                </div>
              }
            </div>
          </Link>
        )}
      </div>
    </>
  )
}

export default Navigation
