import React, { useEffect, useState } from 'react'
import { useOnScroll } from 'hooks'

import { Link, Logo } from 'components'
import { Navigation } from './components'

import cn from 'classnames'
import s from './style.module.sass'
import MenuIcon from 'assets/icons/menu.svg'
import CrossIcon from 'assets/icons/crossWhite.svg'


interface Props {
  statical?: boolean
}

const Header: React.FC<Props> = ({
  statical
}) => {

  const [withBackground, setWithBackground] = useState(false)
  const [showSideMenu, setShowSideMenu] = useState(false)

  const scrollTop = useOnScroll()

  useEffect(() => {
    if (!statical) {
      if (scrollTop > 10) {
        setWithBackground(true)
      } else {
        setWithBackground(false)
      }
    }
  }, [scrollTop])


  const handleToggleSideMenu = () => {
    setShowSideMenu(current => !current)
  }

  return (
    <div className={ cn(s.Header, { [s.withBackground]: withBackground || statical }) }>
      <div className={ s.layer }/>
      <div className={ cn(s.layout, "box-sizing") }>
        <div className={ cn(s.left, "box-sizing") }>
          <Link to="/">
            <Logo size='m'/>
          </Link>
        </div>
        <div className={ cn(s.center, "no-tablet no-mobile box-sizing") }>
          <Navigation/>
        </div>
        <div className={ s.right }>
          <div className={ cn(s.menuButton, "no-desktop") } onClick={ handleToggleSideMenu }>
            <div className={ cn(s.icon, { [s.crossIcon]: showSideMenu }) }>
              <img src={ showSideMenu ? CrossIcon : MenuIcon } alt="menu" className="contained"/>
            </div>
          </div>
        </div>
      </div>
      <div className={ cn(s.sideMenu, "no-desktop scrollable-y", { [s.opened]: showSideMenu, "no-pointer-events": !showSideMenu }) }>
        <div className={ cn(s.layout, "box-sizing") }>
          <Navigation onSelect={ handleToggleSideMenu }/>
        </div>
      </div>
    </div>
  )
}

export default Header
