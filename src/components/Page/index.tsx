import { Header, Footer } from './components'
import React from 'react'
import Head from 'next/head'

import cn from 'classnames'
import s from './style.module.sass'

export interface PageConfig {
  title?: string
  withHeader?: boolean
  withHeaderStatic?: boolean
  withFooter?: boolean
}

const Page: React.FC<PageConfig> = ({
  title,
  withHeader,
  withHeaderStatic,
  withFooter,
  children
}) => {
  return (
    <>
      <Head>
        <title>{ title ? `${ title } - Prominent Properties` : "Prominent Properties" }</title>
      </Head>
      <div className={ cn(s.Page, { [s.withHeaderStatic]: withHeaderStatic }) }>
        { (withHeader || withHeaderStatic) &&
          <div className={ s.header }>
            <Header />
          </div>
        }
        <div className={ s.content }>
          { children }
        </div>
        { withFooter &&
          <div className={ s.footer }>
            <Footer/>
          </div>
        }
      </div>
    </>
  )
}

export default Page
