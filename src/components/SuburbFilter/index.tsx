import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual, sortBy } from 'lodash'
import React, { useEffect, useState } from 'react'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Input, Radio, Select } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'query' | 'suburb'>

interface Props {
  isWidget?: boolean
}

const maxSuburbsToShow = 4

const SuburbFilterRaw: React.FC<Props & RefinementListProvided> = ({
  items,
  searchForItems,
  currentRefinement,
  refine,
  isWidget
}) => {
  const searchState = useReactiveVar(searchVar)
  const { suburb } = searchState

  const [limit, setLimit] = useState(true)
  const sortedItems = sortBy(items, ['-count'])
  const suburbs = sortedItems.slice(0, limit ? maxSuburbsToShow : sortedItems.length).map(item => item.label)
  const suburbsCounts = sortedItems.map(item => item.count)

  useEffect(() => {
    if (!currentRefinement || !isEqual(currentRefinement, suburb)) {
      refine(suburb)
    }
  }, [searchState])

  const onChange = (name: string, value: any) => {
    if (name === 'query') {
      searchForItems(value)
    } else {
      setField('suburb', value)
    }
  }

  const searchForm = useForm<Pick<Options, 'query'>>({
    fields: {
      query: { initialValue: "" }
    }, onChange
  }, [suburb])

  const { values, change } = useForm<Options>({
    fields: {
      suburb: { initialValue: suburb }
    }, onChange
  }, [suburb])

  return (
    <div className={ cn(s.SuburbFilter) }>
      { isWidget ?
          <div>
            <Select multiWithAll name="suburb" options={ suburbs } value={ values.suburb }
                    onChange={ change } placeholder="Suburb" label='Suburb'/>
          </div>
        :
          <>
            <Input secondary name="query" className="mv-2" placeholder="Search by suburb"
                   value={ searchForm.values.query } onChange={ searchForm.change }/>
            <Radio multi className="mb-2" name="suburb" options={ suburbs } textAfter={ suburbsCounts }
                   value={ values.suburb } onChange={ change }/>
            { items.length > maxSuburbsToShow &&
            <p className="bold cursor-pointer text-center mt-2 mb-0" onClick={ () => setLimit(!limit) }>
              { suburbs.length < items.length ? "View more" : "Show fewer" }
            </p>
            }
            <div className={ cn(s.divider, "mt-05 mb-2") }/>
          </>
      }
    </div>
  )
}

const SuburbFilterConnected = connectRefinementList(SuburbFilterRaw)

const SuburbFilter: React.FC<Props> = (props) => (
  <SuburbFilterConnected attribute='suburb_cluster' { ...props }/>
)

export default SuburbFilter
