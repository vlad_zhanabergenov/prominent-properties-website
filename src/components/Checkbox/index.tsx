import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

interface Props {
  name: string
  secondary?: boolean
  label?: string
  white?: boolean
  small?: boolean
  className?: string
  disabled?: boolean
  value?: boolean
  onChange?: (data: { name: string, value: boolean }) => void
}

const Checkbox: React.FC<Props> = ({
  name,
  secondary,
  label,
  white,
  small,
  className,
  disabled,
  value,
  onChange,
}) => {
  const handleChange = () => {
    if (onChange) {
      onChange({ name, value: !value })
    }
  }

  return (
    <div className={ cn(s.Checkbox, "cursor-pointer", { "no-pointer-events": disabled, [s.secondary]: secondary }, className || "") }
         onClick={ handleChange }>
      <div className={ s.wrapper }>
        <div className={ cn( s.checkbox, { [s.white]: white, [s.small]: small, [s.active]: value }) }/>
        <h3 className={ cn(s.label, "m-0", { [s.white]: white, [s.small]: small }) }>{ label }</h3>
      </div>
    </div>
  )
}

export default Checkbox
