import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual } from 'lodash'
import React, { useEffect } from 'react'
import { connectRange } from 'react-instantsearch-dom'

import { Input } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'minGLA'>

interface Props {
  secondary?: boolean
  className?: string
}

const MinGLAFilterRaw: React.FC<Props & any> = ({
  secondary, className,
  min, max,
  currentRefinement, refine
}) => {
  const searchState = useReactiveVar(searchVar)
  const { minGLA } = searchState

  useEffect(() => {
    let parsedMin: number
    const parsedMax = max

    if (minGLA === "" || Number(minGLA) < min) {
      parsedMin = min
    } else if (Number(minGLA) > max) {
      parsedMin = max
    } else {
      parsedMin = Number(minGLA)
    }

    if (typeof parsedMin !== 'undefined' || typeof parsedMax !== 'undefined') {
      const newValue = { min: parsedMin, max: parsedMax }
      if (!isEqual(currentRefinement, newValue)) {
        refine(newValue)
      }
    }
  }, [min, max, searchState])

  const onChange = (name: keyof Options, value: any) => {
    setField(name, value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      minGLA: { initialValue: minGLA }
    }, onChange
  }, [minGLA])

  return (
    <div className={ cn(s.MinGLAFilter, className || "") }>
      <Input secondary={ secondary } name="minGLA" type='number' fontS min={ 0 } placeholder="Min GLA"
             value={ values.minGLA } onChange={ change }/>
    </div>
  )
}

const MinGLAFilterConnected = connectRange(MinGLAFilterRaw)

const MinGLAFilter: React.FC<Props> = (props) => (
  <MinGLAFilterConnected attribute='min_gla' { ...props }/>
)

export default MinGLAFilter
