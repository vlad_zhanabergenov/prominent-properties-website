import { Icon } from 'components'
import React, { memo, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'

// import { Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import CrossIcon from 'assets/icons/crossWhite.svg'

interface IPortalProps {
  id: string
}

interface IModalProps {
  onClose?: () => void
}

const Portal: React.FC<IPortalProps> = memo(({ id, children }) => {
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    setMounted(true)
  }, [])

  if (mounted) {
    return createPortal(children, document.getElementById(id) as Element)
  } else {
    return null
  }
})

const Modal: React.FC<IModalProps> = ({
  children,
  onClose
}) => {
  useEffect(() => {
    const modalDom = document.getElementById("Modal")

    document.body.className = "overflow-hidden"
    if (modalDom) modalDom.className = "open"

    return () => {
      document.body.className = ""
      if (modalDom) modalDom.className = ""
    }
  }, [])

  return (
    <Portal id="Modal">
      <div className={ cn(s.ModalWrapper, "scrollable-y ph-content box-sizing") }>
        <div className={ cn(s.container, "p-4 box-sizing") }>
          { onClose &&
            <div className={ cn(s.close, "cursor-pointer no-select") } onClick={ onClose }>
              <Icon src={ CrossIcon }/>
            </div>
          }
          <div className={ s.layout }>
            { children }
          </div>
        </div>
      </div>
    </Portal>
  )
}

export default Modal
