import { useOnClickOutside } from 'hooks'
import React, { useRef, useState } from 'react'

import { Icon } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import CrossIcon from 'assets/icons/cross.svg'

interface Props {
  name: string
  type?: "email" | "number" | "password" | "search" | "tel" | "text" | "url"
  className?: string
  secondary?: boolean
  linear?: boolean
  transparent?: boolean
  white?: boolean
  fontS?: boolean
  icon?: string
  iconHover?: string
  iconSize?: 'xs' | 's' | 'm' | 'l' | 'xl'
  iconPosition?: 'left' | 'right'
  onIconClick?: () => void
  required?: boolean
  disabled?: boolean
  noAutofill?: boolean
  message?: string | null
  error?: string | null
  label?: string
  labelDark?: boolean
  placeholder?: string
  textBefore?: string
  value?: string | number
  min?: number
  max?: number
  maxLength?: number
  tabIndex?: number
  onChange?: (data: { name: string, value: string | number }) => void
  onFocus?: (name: string) => void
  onBlur?: (name: string) => void
  onPressEnter?: any
  placeholderRight?: boolean
  isSearch?: boolean
  secondarySearch?: boolean
  grayBg?: boolean
}

const Input: React.FC<Props> = ({
  name,
  type,
  className,
  secondary,
  linear,
  transparent,
  white,
  fontS,
  icon,
  iconHover,
  iconSize = "m",
  iconPosition = "left",
  onIconClick,
  required,
  disabled,
  noAutofill,
  message,
  error,
  label,
  labelDark,
  placeholder,
  textBefore,
  value,
  min,
  max,
  maxLength,
  tabIndex,
  onChange,
  onFocus,
  onBlur,
  onPressEnter,
  placeholderRight,
  isSearch,
  secondarySearch,
  grayBg
}) => {
  const [focus, setFocus] = useState(false)
  const ref = useRef<HTMLDivElement>(null)

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    let newValue = target.value

    if (type === 'number') {
      if (typeof min === 'number' && parseInt(newValue) < min) {
        newValue = `${ min }`
      } else if (typeof max === 'number' && parseInt(newValue) > max) {
        newValue = `${ max }`
      }
    }

    if (onChange) {
      onChange({
        name, value: newValue
      })
    }
  }

  const handleFocus = () => {
    setFocus(true)
    if (onFocus) {
      onFocus(name)
    }
  }

  const handleBlur = () => {
    setFocus(false)
    if (onBlur) {
      onBlur(name)
    }
  }

  const handleIconClick = () => {
    if (onIconClick) {
      onIconClick()
    }
  }

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const key = event.keyCode || event.charCode
    if (onPressEnter && key === 13) {
      event.preventDefault()
      onPressEnter()
    }
  }

  const handleClear = () => {
    setFocus(true)
    if (onChange) {
      onChange({
        name, value: ''
      })
    }
  }

  useOnClickOutside(ref, () => {
    setFocus(false)
  })

  const IconWrapper: React.FC = () => {
    if (!icon) { return null }
    return (
      isSearch && value || focus ?
        <div
          className={ cn(s.icon, s.clearIcon, {
            [s.right]: iconPosition === 'right',
            "cursor-pointer": !!onIconClick
          }) }
          onClick={ handleClear }
        >
          <Icon src={ CrossIcon } size={ 'xxs' }/>
        </div>
      :
        <div
          className={ cn(s.icon, {
            [s.right]: iconPosition === 'right',
            "cursor-pointer": !!onIconClick || iconHover,
            [s.withIconHover]: iconHover
          }) }
          onClick={ handleIconClick }
        >
          <Icon src={ icon } size={ iconSize }/>
          { iconHover &&
          <Icon src={ iconHover } size={ iconSize }/>
          }
        </div>
    )
  }

  return (
    <div
      className={ cn(s.Input, {
        [s.secondary]: secondary,
        [s.linear]: linear,
        [s.transparent]: transparent,
        [s.white]: white,
        [s.required]: required,
        [s.grayBg]: grayBg
      }, className || "") }
      ref={ ref }
    >
      { label &&
        <div className={ s.label }>
          <p className={ cn("no-select light", labelDark ? "black" : "white") }>{ label }</p>
        </div>
      }
      <div className={ cn(s.value, { [s.focus]: focus }) }>
        { icon && iconPosition === 'left' && <IconWrapper/> }
        { textBefore && <p className={ cn(s.before, "medium m-0 no-select no-pointer-events") }>{ textBefore }</p> }
        <input
          type={ type || "text" }
          name={ name }
          placeholder={ placeholder || "" }
          value={ value }
          onChange={ handleChange }
          onFocus={ handleFocus }
          onBlur={ handleBlur }
          onKeyPress={ handleKeyPress }
          disabled={ disabled }
          autoComplete={ noAutofill ? "off" : "on" }
          className={ cn({
            [s.withIcon]: icon,
            [s.textBefore]: textBefore,
            [s.textBefore]: fontS,
            [s.placeholderRight]: placeholderRight,
            [s.search]: isSearch,
            [s.withBorder]: isSearch && value || focus,
            [s.secondarySearch]: secondarySearch
          })}
          min={ min }
          max={ max }
          maxLength={ maxLength }
          tabIndex={ tabIndex }
        />
        { required && (typeof value === 'string' && value.length === 0) && <p className={ cn(s.star, "light m-0 no-select no-pointer-events") }>{ placeholder } <span className='red'>*</span></p> }
        { isSearch && value || focus && isSearch ?
            <div
              className={ cn(s.icon, s.clearIcon, {
                [s.right]: iconPosition === 'right',
                "cursor-pointer": !!onIconClick
              }) }
              onClick={ handleClear }
            >
              <Icon src={ CrossIcon } size={ 'xxs' }/>
            </div>
          : icon && iconPosition === 'right' ?
            <div
              className={ cn(s.icon, {
                [s.right]: iconPosition === 'right',
                "cursor-pointer": !!onIconClick || iconHover,
                [s.withIconHover]: iconHover
              }) }
              onClick={ handleIconClick }
            >
              <Icon src={ icon } size={ iconSize }/>
              { iconHover &&
              <Icon src={ iconHover } size={ iconSize }/>
              }
            </div>
          : null
        }
      </div>
      { (error || message) &&
        <p className={ cn(s.message, "small extraLight m-0 ph-1 no-pointer-events box-sizing", white ? 'white' : 'black') }>
          { error || message }
        </p>
      }
    </div>
  )
}

export default Input
