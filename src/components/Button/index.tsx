import { Icon } from 'components'
import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

interface Props {
  full?: boolean
  boldFont?: boolean
  black?: boolean
  disabled?: boolean
  loading?: boolean
  transparent?: boolean
  rounded?: boolean
  className?: string
  icon?: string
  iconHover?: string
  iconSize?: 's' | 'm' | 'l' | 'xl'
  iconRight?: boolean
  largeFont?: boolean
  iconRotate?: 0 | 45 | 90 | 135 | 180 | 225 | 270 | 315
  secondaryHover?: boolean
  onClick?: () => void
}

const Button: React.FC<Props> = ({
  full,
  boldFont,
  black,
  disabled,
  transparent,
  loading,
  rounded,
  className = "",
  icon,
  iconHover,
  iconSize = "m",
  iconRotate = 0,
  onClick,
  secondaryHover,
  iconRight,
  largeFont,
  children
}) => {


  return (
    <div
      className={ cn(s.Button,  "cursor-pointer no-select", {
        [s.black]: black,
        [s.boldFont]: boldFont,
        [s.transparent]: transparent,
        [s.secondaryHover]: secondaryHover,
        [s.disabled]: disabled,
        [s.full]: full,
        [s.rounded]: rounded,
        [s.largeFont]: largeFont,
        "no-pointer-events": disabled || loading,
        "noContent": !children
      }, className) }
      onClick={ onClick }
    >
      { children && children }
      { icon &&
        <div className={ cn(s.icon, { [s.iconRight]: iconRight }) }>
          <Icon src={ icon } size={ iconSize } rotate={ iconRotate } position={ children ? 'right' : null }/>
          { iconHover &&
            <Icon src={ iconHover } size={ iconSize } rotate={ iconRotate } position={ children ? 'right' : null }/>
          }
        </div>
      }
    </div>
  )
}

export default Button
