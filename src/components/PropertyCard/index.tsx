import { useReactiveVar } from '@apollo/client'
import { appVar } from 'cache/vars'
import { CloudImage, Link } from 'components/index'
import { startCase } from 'lodash'
import dynamic from 'next/dynamic'
import React from 'react'

import cn from 'classnames'
import formatNumberWith from 'services/formatNumberWith'
import roundNumber from 'services/roundNumber'
import s from './style.module.sass'

import NoPropertyImage from 'assets/pictures/no-property-image.png'


const PropertyCardSlider = dynamic(
  () => import('components/PropertyCardSlider'),
  { ssr: false }
)

interface Props {
  item: AnyPropertyWithCustomFields,
  isSearch?: boolean,
  className?: string
}

const PropertyCard: React.FC<Props> = ({
  item,
  isSearch,
  className = ''
}) => {
  const itemImage = item.best_image || item.property_images?.[0]?.image_path_url || NoPropertyImage

  const { algoliaStats } = useReactiveVar(appVar)
  const unitsCount = algoliaStats.facets.property_gmaven_key?.[item.property_gmaven_key || ""] || 0

  return (
    <div className={ cn(s.PropertyCard, "box-sizing", className) }>
      <div className={ s.layout }>
        <div className={ s.top }>
          <div className={ s.image }>
            { item.property_images !== undefined && item.property_images?.length > 1 ?
              <PropertyCardSlider images={ item.property_images } data={ item }/>
              :
              <>
                <Link to='property' data={{ property: item }}>
                  <CloudImage
                      className="covered"
                      src={ itemImage }
                      alt={ item.property_name }
                      responsive={{
                        desktop: { w: '500', ar: '1.5' },
                        tablet: { w: '400', ar: '1.5' },
                        mobile: { w: '300', ar: '1.2' }
                      }}
                    />
                </Link>
                <div className={ cn(s.imageLabel, "pt-5 box-sizing") }/>
              </>
            }
          </div>
        </div>
        <Link to='property' data={{ property: item }}>
          <div className={ s.bottom }>
            <div className={ s.label }>
              <p className='small white bold m-0'>
                VIEW <span className='light white'>{ item.dealType === 'toLet' ? 'VACANCY': 'PROPERTY' }</span>
              </p>
            </div>
            { !isSearch &&
              <p className={ cn(s.dealType, "bold big cyan-4 uppercase m-0") }>{ startCase(item.dealType) }</p>
            }
            <p className="bold dark uppercase m-0">{ item.suburb }</p>
            <h3 className="light mt-0 mb-1 limit-string-1">{ item.property_name }</h3>
            <div className={ s.info }>
              <div className={ s.el }>
                <h4 className="light m-0">SQM:</h4>
                <h4 className="light m-0">
                  { item.min_gla !== item.max_gla ?
                    `${ formatNumberWith(' ', item.min_gla || 0) }m² - ${ formatNumberWith(' ', item.max_gla || 0) }m²`
                    :
                    `${ formatNumberWith(' ', item.min_gla || 0) }m²`
                  }
                </h4>
              </div>
              <div className={ s.el }>
                <h4 className="light m-0">
                  { item.dealType === 'toLet' ? 'Gross Rental:' : 'Price' }
                </h4>
                <h4 className="light m-0">
                  { !item.gross_price ?
                    "POA"
                    : item.dealType === 'toLet' ?
                      unitsCount > 1 && item.min_price !== item.max_price ?
                        `R${ formatNumberWith(' ',  roundNumber(item.min_price || 0)) } - ${ formatNumberWith(' ',  roundNumber(item.max_price || 0)) }${ item.dealType === 'toLet' ? ' per m²' : '' }`
                        // `from R${ formatNumberWith(" ", roundNumber(item.gross_price)) } per m²`
                        :
                        `R${ formatNumberWith(" ", roundNumber(item.gross_price)) } per m²`
                      :
                      `R${ formatNumberWith(" ", roundNumber(item.gross_price)) }`
                  }
                </h4>
              </div>
              { item.dealType === 'toLet' &&
                <div className={ s.el }>
                  <h4 className="light m-0">ESC:</h4>
                  <h4 className="light m-0">
                    TBC
                    {/*{ !item.min_esc && !item.max_esc ?*/}
                    {/*    'TBC'*/}
                    {/*  :*/}
                    {/*    item.min_esc === item.max_esc*/}
                    {/*  ?*/}
                    {/*    `${ item.min_esc }%`*/}
                    {/*  :*/}
                    {/*    `from ${ item.min_esc }%`*/}
                    {/*}*/}
                  </h4>
                </div>
              }
            </div>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default PropertyCard
