import React from 'react'

import s from './style.module.sass'
import cn from 'classnames'

interface Props {
  name: string
  options: string[]
  multi?: boolean
  multiWithAll?: boolean
  secondary?: boolean
  label?: string
  white?: boolean
  small?: boolean
  disabled?: boolean
  textAfter?: Array<string | number>
  value: string | string[]
  className?: string
  onChange?: (data: { name: string, value: string | string[] }) => void
}

const Radio: React.FC<Props> = ({
  name,
  options,
  multi,
  multiWithAll,
  secondary,
  white,
  small,
  disabled,
  textAfter,
  value,
  className,
  onChange,
}) => {
  const handleChange = (v: string) => {
    let newValue: Props['value'] = v

    if (typeof value === 'object') {
      if (multiWithAll && v === "All") {
        newValue = [v]
      } else if (multiWithAll) {
        const filteredValue = value.filter(item => item !== "All")
        if (filteredValue.includes(v)) {
          newValue = filteredValue.filter(item => item !== v)
        } else {
          if (options.length === [...filteredValue, v].length) {
            newValue = ["All"]
          } else {
            newValue = [...filteredValue, v]
          }
        }
      } else {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          newValue = [...value, v]
        }
      }
    }

    if (onChange) {
      onChange({ name, value: newValue })
    }
  }

  return (
    <div className={ cn(s.Radio, "cursor-pointer", { "no-pointer-events": disabled, [s.secondary]: secondary }, className) }>
      { multiWithAll &&
        <div className={ cn(s.item, "mv-1") } onClick={ () => handleChange("All") }>
          <div className={ cn(s.checkbox, { [s.white]: white, [s.small]: small, [s.active]: value.includes("All") }) }/>
          <h3 className={ cn(s.label, " black m-0 limit-string-1 regular light", { [s.white]: white }) }>{ "All" }</h3>
          { textAfter &&
            <h3 className={ cn(s.textAfter, "regular small grey-3 m-0") }>{ textAfter[0] }</h3>
          }
        </div>
      }
      { options && options.map((i, num) => {
        const active = (multi || multiWithAll) ? value?.includes(i) : value === i
        return (
          <div key={ i + num } className={ cn(s.item, "mv-1") } onClick={ () => handleChange(i) }>
            <div className={ cn(s.checkbox, { [s.white]: white, [s.small]: small, [s.active]: active }) }/>
            <h3 className={ cn(s.label, "black m-0 limit-string-1 regular light", { white, small }) }>{ i }</h3>
            { textAfter &&
              <h3 className={ cn(s.textAfter, "regular small grey-3 m-0") }>{ textAfter[multiWithAll ? num + 1 : num] || textAfter[0] }</h3>
            }
          </div>
        )
      })}
    </div>
  )
}

export default Radio
