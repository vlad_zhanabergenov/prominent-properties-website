import React from 'react'

import { Icon } from 'components/index'

import s from './style.module.sass'
import SpinnerIcon from 'assets/icons/spinner.svg'
import SpinnerWhiteIcon from 'assets/icons/spinnerWhite.svg'

interface Props {
  size?: 's' | 'm' | 'l' | 'xl'
  white?: boolean
}

const Loading: React.FC<Props> = ({
  size = 'm',
  white
}) => {
  return (
    <div className={ s.Loading }>
      <div className={ s.spinner }>
        <Icon src={ white ? SpinnerWhiteIcon : SpinnerIcon } size={ size }/>
      </div>
    </div>
  )
}

export default Loading
