import _ from 'lodash'
import React from 'react'

interface Props {
  src?: string
  alt?: string
  className?: string
  responsive?: {
    desktopLarge?: ResponsiveParams
    desktop?: ResponsiveParams
    tablet?: ResponsiveParams
    mobile?: ResponsiveParams
  }
}

type ResponsiveParams = {
  q?: string
  w?: string
  h?: string
  ar?: string
}

const requiredParams: Partial<ResponsiveParams> & { f: string } = {
  q: 'auto',
  f: 'auto'
}

const concurrentParams: [keyof ResponsiveParams, keyof ResponsiveParams][] = [
  ['h', 'ar']
]

const CloudImage: React.FC<Props> = ({
                                       src= "",
                                       alt,
                                       className = "",
                                       responsive
                                     }) => {
  const urlArray = src.split('/')
  const paramsIndex = urlArray.findIndex(i => i === 'upload') + 1
  const paramsPairs = urlArray[paramsIndex].includes('_') ? urlArray[paramsIndex].split(',') : []
  const params = _.fromPairs(paramsPairs.map(i => i.split('_')))

  const parseSrc = (customParams: object = {}) => {
    if (paramsIndex <= 0) return src

    const urlArrayClone = [...urlArray]

    const allParams: { [key: string]: string } = {
      ...params,
      ...requiredParams,
      ...customParams
    }

    const mergedParams = _.pickBy(allParams, (_value, key) => {
      let result = true
      concurrentParams.forEach(i => result = !(key === i[0] && allParams.hasOwnProperty(i[1])))
      return result
    })

    urlArrayClone[paramsIndex] = _.toPairs(mergedParams).map(i => i.join('_')).join(',')

    return urlArrayClone.join('/')
  }

  if (!src) {
    return null
  } else if (responsive) {
    return (
      <picture>
        <source media="(min-width: 1600px)" srcSet={ parseSrc({ ...responsive.desktopLarge || {} }) }/>
        <source media="(min-width: 961px)" srcSet={ parseSrc({ ...responsive.desktop || {} }) }/>
        <source media="(min-width: 768px)" srcSet={ parseSrc({ ...responsive.tablet || {} }) }/>
        <source media="(max-width: 767px)" srcSet={ parseSrc({ ...responsive.mobile || {} }) }/>
        <img src={ parseSrc({ ...responsive.desktop || {} }) } alt={ alt } className={ className }/>
      </picture>
    )
  } else {
    return (
      <img src={ parseSrc() } alt={ alt } className={ className }/>
    )
  }
}

export default CloudImage
