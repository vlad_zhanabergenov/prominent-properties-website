import React, { useEffect, useRef, useState } from 'react'
import { useOnClickOutside } from 'hooks'

import { Icon } from 'components'

import s from './style.module.sass'
import cn from 'classnames'
import ArrowIcon from 'assets/icons/arrowBlack.svg'

interface Props {
  name: string
  options: string[]
  multi?: boolean
  multiWithAll?: boolean
  secondary?: boolean
  label?: string
  labelDark?: boolean
  placeholder?: string
  small?: boolean
  disabled?: boolean
  value: string | string[]
  className?: string
  onChange?: (data: { name: string, value: string | string[] }) => void
  onBlur?: (name: string) => void
}

const Select: React.FC<Props> = ({
  name,
  options,
  multi,
  multiWithAll,
  secondary,
  label,
  labelDark,
  placeholder,
  small,
  disabled,
  value,
  className,
  onChange,
  onBlur,
}) => {
  const [listOpen, setListOpen] = useState(false)
  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    value && onBlur && onBlur(name)
  }, [value])

  useOnClickOutside(ref, () => {
    listOpen && setListOpen(false)
  })

  const toggleList = () => {
    if (ref.current) {
      if (!listOpen) {
        setListOpen(true)
      } else {
        setListOpen(false)
      }
    }
  }

  const handleChange = (v: string) => {
    let newValue: Props['value'] = v

    if (typeof value === 'object') {
      if (multiWithAll && v === "ALL") {
        newValue = []
      } else if (multiWithAll) {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          if (options.length === value.length + 1) {
            newValue = []
          } else {
            newValue = [...value, v]
          }
        }
      } else {
        if (value.includes(v)) {
          newValue = value.filter(item => item !== v)
        } else {
          newValue = [...value, v]
        }
      }
    }

    if (onChange) {
      onChange({ name, value: newValue })
    }

    !multi && !multiWithAll && setListOpen(false)
  }

  return (
    <div className={ cn(s.Select, "cursor-pointer", { "no-pointer-events": disabled, [s.secondary]: secondary }, className) }>
      <div ref={ ref } className={ cn(s.wrapper, { [s.open]: listOpen, [s.disabled]: disabled }) }>
        { label &&
          <div className={ cn(s.label, "text-left") }>
            <p className={ cn("no-select light m-0", labelDark ? "black" : "grey-3") }>{ label }</p>
          </div>
        }
        <div className={ s.input } onClick={ () => toggleList() }>
          <h3 className={ cn("no-select m-0 light small smallMobile", { small }) }>
            { value ?
                (multi || multiWithAll) ?
                  value[0] || (multiWithAll ? "All" : placeholder)
                : value
              : placeholder
            }
          </h3>
          { ((multi || multiWithAll) && value?.length && value.length > 1) ?
              <h3 className={ cn(s.indicator, "no-select m-0 pv-05 pl-05 small light smallMobile", { small }) }>{ `+${ value.length - 1 }` }</h3>
            : null
          }
          <Icon src={ ArrowIcon } size='xs' rotate={ listOpen ? 90 : 0 }/>
        </div>
        <div className={ cn(s.options, "scrollbar-hidden") }>
          <div className={ cn(s.list) }>
            { multiWithAll &&
              <div onClick={ () => handleChange("ALL") } className={ cn(s.item, { [s.active]: !value.length }) }>
                <h3 className={ cn("no-select text-left light small m-0 smallMobile") }>All</h3>
              </div>
            }
            { options.map(item => {
              const active = (multi || multiWithAll) ? value?.includes(item) : value === item
              return (
                <div key={ item } onClick={ () => handleChange(item) } className={ cn(s.item, { [s.active]: active }) }>
                  <h3 className={ cn("no-select text-left small light m-0 smallMobile") }>{ item }</h3>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Select
