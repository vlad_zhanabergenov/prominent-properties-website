import apolloClient from 'services/apollo'
import { sortBy } from 'lodash'
import { QUERY_PROPERTY_TO_LET } from 'api/queries'
import addDealTypeField from 'services/addDealTypeField'

export default async function getPropertyToLet(property_gmaven_key: string) {
  let items: AnyProperty[] = []

  const { data } = await apolloClient.query<QueryPropertyToLetRes>({
    query: QUERY_PROPERTY_TO_LET,
    variables: { property_gmaven_key }
  }).catch(() => ({ data: null }))

  if (data?.queryPropertyToLet) {
    items = sortBy(addDealTypeField(data.queryPropertyToLet), ['gross_price'])
  }

  return items
}
