import _ from 'lodash'
import apolloClient from 'services/apollo'
import { QUERY_RELATED_FOR_SALE } from 'api/queries'
import addDealTypeField from 'services/addDealTypeField'

export default async function getRelatedForSale(
  gmaven_mapped_key: string,
  category: string,
  suburb: string
) {
  let items: AnyProperty[] = []

  const { data } = await apolloClient.query<QueryRelatedForSaleRes>({
    query: QUERY_RELATED_FOR_SALE,
    variables: { category, gmaven_mapped_key, suburb }
  }).catch(() => ({ data: null }))

  if (data?.queryRelatedForSale) {
    items = addDealTypeField(data.queryRelatedForSale)
  }

  return _.uniqBy(items, 'property_name')
}
