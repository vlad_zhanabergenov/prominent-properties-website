import content from 'services/contentful'

export default async function getHeroCarouselItems() {
  const items: HeroCarouselItem[] = []

  const res = await content.getEntries<HeroCarouselItemContentful>({ content_type: "siteBanner" })

  if (res.items) {
    console.log(res.items, 'items')
    res.items.forEach(({ fields }, num) => {
      items.push({
        id: `${ num }`,
        title: fields.title,
        image: fields.sliderImage,
        mobileImage: fields.mobileImage
      })
    })
  }

  return items
}
