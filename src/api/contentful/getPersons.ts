import content from 'services/contentful'

export default async function getPersons() {
  const items: PersonItem[] = []

  const res = await content.getEntries<PersonItemContentful>({ content_type: "person" })

  if (res.items) {
    res.items.forEach(({ fields }, num) => {
      console.log(fields)
      items.push({
        id: `${ num + 1 }`,
        name: fields.name,
        image: fields.image.fields.file.url,
        title: fields.title,
        shortBio: fields.shortBio,
        email: fields.email,
        phone: fields.phone,
        linkedIn: fields.linkedIn,
        twitter: fields.twitter
      })
    })
  }

  return items
}
