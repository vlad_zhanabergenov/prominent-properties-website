import searchClient from 'services/algoliaAdmin'
import { groupBy } from 'lodash'
import slugParser, { StaticPropertyFromSlug } from 'services/slugParser'

export default async function getStaticProperties(index: 'ToLet' | 'ForSale') {
  let results: StaticPropertyFromSlug[] = []

  const searchIndex = searchClient.initIndex(index)
  const attributes: string[] = ['property_slug']

  if (index === 'ToLet') {
    attributes.push('unit_slug')
  } else {
    attributes.push('property_type')
  }

  return searchIndex.browseObjects<AnyProperty>({
    query: '',
    attributesToRetrieve: attributes,
    batch: res => {
      if (index === 'ToLet') {
        const groupedList = Object.values(groupBy(res, 'property_slug')) as PropertyToLet[][]

        groupedList.forEach(group => {
          results.push(slugParser.parse(group[0].property_slug!, false))
          group.forEach(i => {
            results.push(slugParser.parse(i.unit_slug!, true))
          })
        })
      } else if (index === 'ForSale') {
        (res as PropertyForSale[]).forEach(i => {
          const isUnit = 'property_type' in i && i.property_type === 'property_unit'
          results.push(slugParser.parse(i.property_slug!, isUnit))
        })
      }
    }
  })
  .then(() => results)
  .catch(() => results)
}
