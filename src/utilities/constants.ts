export const navItems: NavItem[] = [
  { id: 'home', name: "Home", path: "/" },
  { id: 'about-us', name: "About us", path: "/about" },
  { id: 'search', name: "Property search", path: "/search" },
  { id: 'people', name: "People", path: "/people" },
  { id: 'contact', name: "Contact", path: "/contact" }
]

export enum searchIndexes {
  Buy = "ForSale",
  Rent = "ToLet"
}

export const searchConfig = {
  distinct: 100,
  facetingAfterDistinct: false,
  hitsPerPage: 8,
  attributesToRetrieve: ["*"],
  facets: ['property_gmaven_key'],
  maxValuesPerFacet: 10000
}

export const searchOptions: HeroSearchOptions = {
  dealType: ["For Sale", "To Let"]
}
