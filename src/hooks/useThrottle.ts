import { throttle } from 'lodash'
import { useCallback } from 'react'

export default function useThrottle(
  callback: (...args: any) => any,
  deps: any[] = [],
  options: { leading?: boolean, trailing?: boolean } = { leading: true, trailing: true },
  wait: number = 1300
) {
  return useCallback(throttle(callback, wait, options), deps)
}
