import Router from 'next/router'
import { useEffect } from 'react'

export default function useScrollPositionKeeper() {
  useEffect(() => {
    if ('scrollRestoration' in window.history) {
      window.history.scrollRestoration = 'manual'
      const cachedScrollPositions: any = []
      let shouldScrollRestore: { x: number, y: number } | false

      Router.events.on("routeChangeStart", () => {
        cachedScrollPositions.push([window.scrollX, window.scrollY])
      })

      Router.events.on("routeChangeComplete", () => {
        if (shouldScrollRestore) {
          const { x, y } = shouldScrollRestore
          window.scrollTo(x, y)
          shouldScrollRestore = false
        }
      })

      Router.beforePopState(() => {
        if (cachedScrollPositions.length) {
          const [ x, y ] = cachedScrollPositions.pop()
          shouldScrollRestore = { x, y }
        }
        return true
      })
    }
  },[])
}
