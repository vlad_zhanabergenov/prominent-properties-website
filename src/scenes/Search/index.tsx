import { resetFields } from 'cache/mutations/search'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { searchVar } from 'cache/vars'

import { Button, Breadcrumbs, Search } from 'components'
import { useReactiveVar } from '@apollo/client'
import searchStateParser from 'services/searchStateParser'
import { Pagination, Results, SearchFilters, SearchFiltersModal } from './components'

import s from './style.module.sass'
import cn from 'classnames'
import FiltersIcon from 'assets/icons/filters.svg'

const SearchScene: React.FC = () => {
  const router = useRouter()
  const searchState = useReactiveVar(searchVar)
  const [mount, setMount] = useState(false)
  const [showFiltersModal, setShowFiltersModal] = useState(false)

  useEffect(() => {
    if (!mount && router.isReady) {
      if (Object.keys(router.query).length) {
        const searchStateFromParams = searchStateParser.decode(router.query)
        resetFields(searchStateFromParams)
      }
      setMount(true)
    }
  }, [router])

  useEffect(() => {
    if (mount) {
      router.push(
        `${ window.location.pathname }?${ searchStateParser.encode(searchState) }`,
        undefined,
        { shallow: true }
      )
    }
  }, [searchState])

  return (
    <div className={ s.SearchScene }>
      <div className={ s.layout }>
        <div className={ s.header}>
          <Breadcrumbs items={ searchState.dealType ? [searchState.dealType] : ['Search'] }/>
        </div>
        <div className={ s.searchBar }>
          <Search/>
          { showFiltersModal &&
            <SearchFiltersModal onClose={ () => setShowFiltersModal(false) }/>
          }
        </div>
        <div className={ cn(s.content) }>
          <div className={ cn(s.sidebar, "pb-2 no-mobile") }>
            { !showFiltersModal && <SearchFilters/> }
          </div>
          <div className={ cn(s.results) }>
            <Results/>
            <Pagination className="mt-3"/>
          </div>
        </div>
        <div className={ cn(s.filterButtonContainer, "no-desktop no-tablet box-sizing") }>
          <div className={ s.layer }/>
          <Button className={ cn(s.filterButton) } rounded icon={ FiltersIcon } iconSize='l'
                  onClick={ () => setShowFiltersModal(true) } iconRight largeFont>
            Filter
          </Button>
        </div>
      </div>
    </div>
  )
}

export default SearchScene
