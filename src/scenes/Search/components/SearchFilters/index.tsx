import React, { useState } from 'react'
import { useReactiveVar } from '@apollo/client'
import { resetFields } from 'cache/mutations/search'
import { appVar, searchVar } from 'cache/vars'
import { connectStateResults } from 'react-instantsearch-core'
import initialState from 'cache/initialState'

import { CategoryFilter, SuburbFilter, DealTypeFilter } from './components'
import {  PriceFilter, MinGLAFilter, MaxGLAFilter, Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

const SearchFilters: React.FC = () => {
  const { algoliaReady } = useReactiveVar(appVar)
  const { dealType } = useReactiveVar(searchVar)
  const [active, setActive] = useState<String[]>(['category', 'suburb', 'refine'])

  const handleResetFilters = () => {
    resetFields({ ...initialState.search, dealType })
  }

  if (!algoliaReady) {
    return null
  }

  const changeActive = (name: string) => {
    if (active.includes(name)) {
      setActive(active.filter(item => item !== name))
    } else {
      setActive([...active, name])
    }
  }

  return (
    <div className={ cn(s.SearchFilters) }>
      <div className={ s.layout }>
        <h2 className="no-mobile bold mt-0 mb-1 black">Filter</h2>
        {/*<SearchStats/>*/}
        <h3 className="small light black cursor-pointer display-inline mt-05 mb-2" onClick={ handleResetFilters }>Reset</h3>
        <DealTypeFilter />
        <div className={ cn(s.form, 'no-mobile') }>
          <div className={ cn(s.divider) }>
            <h3 className='white small medium'>Category</h3>
          </div>
          <CategoryFilter/>
          <div className={ cn(s.divider) }>
            <h3 className='white small medium'>Suburb</h3>
          </div>
          <SuburbFilter/>
          <div className={ cn(s.divider) }>
            <h3 className='white small medium'>Refine</h3>
          </div>
          <PriceFilter secondary/>
          <MinGLAFilter secondary className="mv-05"/>
          <MaxGLAFilter secondary/>
          <div className={ cn(s.line, "mv-3") }/>
        </div>
        <div className={ cn(s.form, 'no-desktop no-tablet') }>
          <div className={ cn(s.divider) } onClick={ () => changeActive('category') }>
            <h3 className='white small medium'>Category</h3>
            <Icon src={ ArrowIcon } rotate={ active.includes('category') ? 180 : 90 }/>
          </div>
          { active.includes('category') && <CategoryFilter/> }
          <div className={ cn(s.divider) } onClick={ () => changeActive('suburb') }>
            <h3 className='white small medium'>Suburb</h3>
            <Icon src={ ArrowIcon } rotate={ active.includes('suburb') ? 180 : 90 }/>
          </div>
          { active.includes('suburb') && <SuburbFilter/> }
          <div className={ cn(s.divider) } onClick={ () => changeActive('refine') }>
            <h3 className='white small medium'>Refine</h3>
            <Icon src={ ArrowIcon } rotate={ active.includes('refine') ? 180 : 90 }/>
          </div>
          { active.includes('refine') &&
            <>
              <PriceFilter secondary/>
              <MinGLAFilter secondary className="mv-05"/>
              <MaxGLAFilter secondary/>
            </>
          }
          <div className={ cn(s.line, "mv-3 no-mobile") }/>
        </div>
      </div>
    </div>
  )
}

export default connectStateResults(SearchFilters)
