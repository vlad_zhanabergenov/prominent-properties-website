import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { Icon } from 'components'
import { useForm } from 'hooks'
import React from 'react'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrowGrey.svg'

type Options = Pick<AppCache['search'], 'dealType'>

const DealTypeFilter: React.FC = () => {
  const { dealType } = useReactiveVar(searchVar)

  const onChange = (_name: keyof Options, value: any) => {
    setField('dealType', value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      dealType: { initialValue: dealType }
    }, onChange
  }, [dealType])

  const changeType = (value: string) => {
    change({ name: 'dealType', value })
  }

  return (
    <div className={ cn(s.DealTypeFilter) }>
      <div className={ s.layout }>
        <div className={ cn(s.left, { [s.active]: values.dealType === 'To Let' }) }>
          <div onClick={ () => changeType('To Let') } className='cursor-pointer'>
            <Icon src={ ArrowIcon } rotate={ 270 } size='xxs'/>
          </div>
          <h3 className={ cn('grey-2 small m-0 regular cursor-pointer') } onClick={ () => changeType('To Let') }>To Let</h3>
        </div>
        <div className={ cn(s.right, { [s.active]: values.dealType === 'For Sale' }) }>
          <h3 className='grey-2 small m-0 regular cursor-pointer' onClick={ () => changeType('For Sale') }>For Sale</h3>
          <div onClick={ () => changeType('For Sale') } className='cursor-pointer'>
            <Icon src={ ArrowIcon } rotate={ 90 } size='xxs'/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DealTypeFilter
