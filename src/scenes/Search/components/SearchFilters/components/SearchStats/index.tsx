import React from 'react'
import { StatsProvided } from 'react-instantsearch-core'
import { connectStats } from 'react-instantsearch-dom'
import formatNumberWith from 'services/formatNumberWith'

import cn from 'classnames'
import s from './style.module.sass'

const SearchStatsRaw: React.FC<StatsProvided> = ({
  nbHits
}) => {
  return (
    <div className={ cn(s.SearchStats) }>
      <h3 className="no-mobile small black light m-0">{ `${ formatNumberWith(" ", nbHits) || 0 } Results` }</h3>
    </div>
  )
}

const SearchStatsConnected = connectStats(SearchStatsRaw)

const SearchStats: React.FC = () => (
  <SearchStatsConnected/>
)

export default SearchStats
