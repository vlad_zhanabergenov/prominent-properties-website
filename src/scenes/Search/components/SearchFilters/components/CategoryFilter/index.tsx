import { useReactiveVar } from '@apollo/client'
import { setField } from 'cache/mutations/search'
import { searchVar } from 'cache/vars'
import { useForm } from 'hooks'
import { isEqual, sortBy } from 'lodash'
import React, { useEffect, useState } from 'react'
import { RefinementListProvided } from 'react-instantsearch-core'
import { connectRefinementList } from 'react-instantsearch-dom'

import { Radio } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

type Options = Pick<AppCache['search'], 'propertyType'>

const maxPropertyTypesToShow = 6

const CategoryFilterRaw: React.FC<RefinementListProvided> = ({ items, currentRefinement, refine }) => {
  const searchState = useReactiveVar(searchVar)
  const { propertyType } = searchState

  const [limit, setLimit] = useState(true)
  const sortedItems = sortBy(items, ['count']).reverse()
  const propertyTypes = sortedItems.slice(0, limit ? maxPropertyTypesToShow : sortedItems.length).map(item => item.label)
  const propertyTypesCounts = sortedItems.map(item => item.count)

  useEffect(() => {
    if (!currentRefinement || !isEqual(currentRefinement, searchState.propertyType)) {
      refine(propertyType)
    }
  }, [searchState])

  const onChange = (_name: string, value: any) => {
    setField('propertyType', value)
  }

  const { values, change } = useForm<Options>({
    fields: {
      propertyType: { initialValue: propertyType }
    }, onChange
  }, [propertyType])

  if (!items.length) {
    return null
  }

  return (
    <div className={ cn(s.CategoryFilter) }>
      <Radio multi className="mb-2" name="propertyType" options={ propertyTypes } textAfter={ propertyTypesCounts }
             value={ values.propertyType } onChange={ change }/>
      { items.length > maxPropertyTypesToShow &&
        <>
          <p className="regular cursor-pointer text-center mt-2 mb-0" onClick={ () => setLimit(!limit) }>
            { propertyTypes.length < items.length ? "View more" : "Show fewer" }
          </p>
          <div className={ cn(s.divider, "mt-05 mb-2") }/>
        </>
      }
    </div>
  )
}

const CategoryFilterConnected = connectRefinementList(CategoryFilterRaw)

const CategoryFilter: React.FC = () => (
  <CategoryFilterConnected attribute='property_category'/>
)

export default CategoryFilter
