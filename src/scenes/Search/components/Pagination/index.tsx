import React, { useEffect } from 'react'
import { connectPagination } from 'react-instantsearch-dom'
import { useReactiveVar } from '@apollo/client'

import  { Icon } from 'components'
import { searchVar } from 'cache/vars'
import setField from 'cache/mutations/search/setField'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

interface Props {
  className?: string
}

const Pagination: React.FC<Props & any> = ({
  className,
  currentRefinement: current,
  nbPages,
  refine
}) => {
  const pages = Array.from(Array(nbPages).keys()).map(i => i + 1)
  const pagesToRender = pages.length > 5 ? pages.slice(getOffset('start'), getOffset('end')) : pages

  const { page } = useReactiveVar(searchVar)

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
    refine(page)
  }, [page])

  useEffect(() => {
    if (Number(page) > (pages.length || 1)) {
      setField('page', pages.length || 1)
    }
  }, [page, pages])

  function getOffset(type: 'start' | 'end') {
    const offsetStart = current < 4 ? 0 : current + 2 >= pages.length ? pages.length - 5 : current - 3

    if (type === 'start') {
      return offsetStart
    }

    return offsetStart + 5 < pages.length ? offsetStart + 5 : pages.length
  }

  const handleChange = (page: number) => {
    setField('page', page)
  }

  if (pages.length < 2) {
    return null
  }

  return (
    <div className={ cn(s.Results, className || "") }>
      <div className={ s.layout }>
        { current > 1 &&
        <div className={ cn(s.item, 'mr-1') } onClick={ () => handleChange(current - 1) }>
          <Icon src={ ArrowIcon } rotate={ 270 }/>
        </div>
        }
        { pagesToRender.map(num =>
          <h3 key={ num } onClick={ () => handleChange(num) }
              className={ cn("extraLight mv-0 mh-1 cursor-pointer", { 'bold': current === num }) }>
            { num }
          </h3>
        )}
        { current < pages.length &&
          <div className={ cn(s.item, 'ml-1') } onClick={ () => handleChange(current + 1) }>
            <Icon src={ ArrowIcon } rotate={ 90 }/>
          </div>
        }
      </div>
    </div>
  )
}

export default connectPagination(Pagination)
