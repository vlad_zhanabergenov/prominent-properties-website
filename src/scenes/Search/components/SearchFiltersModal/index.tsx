import React from 'react'

import {  SearchFilters } from '../../components'
import { Button, Modal } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  onClose: () => void
}

const SearchFiltersModal: React.FC<Props> = ({ onClose }) => {

  return (
    <Modal onClose={ onClose }>
      <div className={ s.SearchFiltersModal }>
        <div className={ s.layout }>
          <div className={ s.wrapper }>
            <SearchFilters/>
            <div className={ cn(s.buttons, "ph-4 box-sizing") }>
              <Button onClick={ onClose } rounded black>Apply Filters</Button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default SearchFiltersModal
