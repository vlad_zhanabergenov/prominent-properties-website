import { useReactiveVar } from '@apollo/client'
import React, { useEffect, useState } from 'react'
import { HitsProvided } from 'react-instantsearch-core'
import { connectHits } from 'react-instantsearch-dom'

import { PropertyCard } from 'components'
import { appVar, searchVar } from 'cache/vars'
import { resetFields } from 'cache/mutations/search'
import initialState from 'cache/initialState'

import cn from 'classnames'
import addDealTypeField from 'services/addDealTypeField'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'
import s from './style.module.sass'

const Results: React.FC<HitsProvided<AnyProperty>> = ({ hits }) => {
  const { algoliaReady } = useReactiveVar(appVar)
  const { dealType } = useReactiveVar(searchVar)
  const [list, setList] = useState<AnyProperty[]>([])

  useEffect(() => {
    setList(groupPropertiesByGmavenKey(addDealTypeField(hits)))
  }, [hits])

  if (!algoliaReady) {
    return null
  }

  const handleResetFilters = () => {
    resetFields({ ...initialState.search, dealType })
  }

  return (
    <div className={ cn(s.Results) }>
      <div className={ s.layout }>
        { hits.length ?
            <div className={ s.list }>
              { list.map(item =>
                <PropertyCard key={ item.objectID } item={ item } isSearch/>
              )}
            </div>
          :
          <h3 className="medium mv-10 text-center">Your search returned no results. Please <span className='cursor-pointer bold cyan-4' onClick={ handleResetFilters }>reset</span> your filter and try again.</h3>
        }
      </div>
    </div>
  )
}

export default connectHits(Results)
