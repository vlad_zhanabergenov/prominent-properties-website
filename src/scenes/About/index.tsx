import { CloudImage } from 'components'
import React from 'react'
import { Information } from './componenets'

import s from './style.module.sass'
// import cn from 'classnames'
import AboutUsPicture from 'assets/pictures/aboutUs.png'
import AboutUsMobilePicture from 'assets/pictures/aboutUsMobile.png'

const About = () => {
  return (
    <div className={ s.About }>
      <div className={ s.layout }>
        <div className={ s.left }>
          <div className={ s.top }>
            <h2 className='fontSecondary big white m-0 uppercase no-mobile'>A Solid<br/> Basis for Success.</h2>
            <h1 className='fontSecondary white m-0 uppercase no-desktop no-tablet'>A Solid<br/> Basis for Success.</h1>
            <p className='white small light m-0 no-mobile'>Founded in 2005, Prominent Properties is
              a 100% black-empowered, women-driven
              company, providing an integrated service
              across the commercial, industrial and
              retail property markets. We aspire to be
              the largest independent property
              management group in Southern Africa.</p>
            <p className='white big light m-0 no-desktop no-tablet'>Founded in 2005, Prominent Properties is
              a 100% black-empowered, women-driven
              company, providing an integrated service
              across the commercial, industrial and
              retail property markets. We aspire to be
              the largest independent property
              management group in Southern Africa.</p>
          </div>
          <div className={ s.bottom }>
            <div className={ s.image }>
              <CloudImage
                className="contained no-mobile"
                src={ AboutUsPicture }
                alt='About Us'
                responsive={{
                  desktopLarge: { w: '800' },
                  desktop: { w: '600' },
                  tablet: { w: '500' },
                  mobile: { w: '400' }
                }}
              />
              <CloudImage
                className="contained no-desktop no-tablet"
                src={ AboutUsMobilePicture }
                alt='About Us'
                responsive={{
                  desktopLarge: { w: '800' },
                  desktop: { w: '600' },
                  tablet: { w: '500' },
                  mobile: { w: '400' }
                }}
              />
            </div>
          </div>
        </div>
        <div className={ s.right }>
          <Information/>
        </div>
      </div>
    </div>
  )
}

export default About
