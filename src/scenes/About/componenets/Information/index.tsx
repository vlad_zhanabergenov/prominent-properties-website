import { ButtonDownload } from 'components'
import React from 'react'

import cn from 'classnames'
import s from './style.module.sass'

const data = [
  { id: 1, title: 'STRATEGY', description: ['Business Development', 'Development Management', 'Corporate Services'] },
  { id: 2, title: 'PROPERTY MANAGEMENT', description: ['Retail, Commercial and Industrial', 'Management', 'Key Accounts Management', 'Marketing Valuations'] },
  { id: 3, title: 'OPERATIONS', description: ['Tenant Installation', 'Project Management', 'Facilities Management'] }
]

const Information: React.FC = () => {
  return (
    <div className={ cn(s.Information) }>
      <div className={ s.layout }>
        <div className={ s.section1 }>
          { data.map(item =>
            <div key={ item.id } className={ s.item }>
              <h3 className='cyan-4 semiBig m-0 no-mobile'>{ item.title }</h3>
              <h2 className='cyan-4 semiBig m-0 no-tablet no-desktop'>{ item.title }</h2>
              <div className={ s.line }/>
              <div className={ s.wrapper }>
                { item.description.map(i =>
                  <React.Fragment key={ i }>
                    <p className='small medium black mb-05 mt-0 no-mobile'>{ i }</p>
                    <p className='big medium black mb-1 mt-0 no-desktop no-tablet'>{ i }</p>
                  </React.Fragment>
                )}
              </div>
            </div>
          )}
        </div>
        <div className={ s.section2 }>
          <h3 className='black semiBig bold m-0'>TEAM PLAYERS AND A LEADING EDGE IT PLATFORM</h3>
          <p className='small regular black mt-0 mb-05 no-mobile'>Formed in 2005, Prominent Properties is a 100% black woman owned and managed company. It comprises
            individuals with experience in the property sector spanning a period of over 80 years. It provides an
            integrated services across the commercial, industrial and retail markets.</p>
          <p className='small regular black m-0 no-mobile'>Prominent Properties has a team of specialists with a wealth of experience to assist clients with any aspects
            of property and facilities management. Our highly trained experts are backed by an integrated information
            technology management system</p>
          <p className='big regular black mt-0 mb-05 no-desktop no-tablet'>Formed in 2005, Prominent Properties is a 100% black woman owned and managed company. It comprises
            individuals with experience in the property sector spanning a period of over 80 years. It provides an
            integrated services across the commercial, industrial and retail markets.</p>
          <p className='big regular black m-0 no-desktop no-tablet'>Prominent Properties has a team of specialists with a wealth of experience to assist clients with any aspects
            of property and facilities management. Our highly trained experts are backed by an integrated information
            technology management system</p>
          <h3 className='black semiBig bold m-0'>THE THREE PILLARS</h3>
          <p className='small regular black mt-0 no-mobile'>Benchmarked against top property and facilities management practices, Prominent Properties business is
            underpinned by three principles:</p>
          <p className='big regular black mt-0 mb-1 no-tablet no-desktop'>Benchmarked against top property and facilities management practices, Prominent Properties business is
            underpinned by three principles:</p>
          <div className={ s.list }>
            <p className='small bold black m-0 no-mobile'>Highly skilled, professional staff who are constantly evolving to meet the current
              requirements within the sector;</p>
            <p className='small bold black m-0 no-mobile'>A high-tech management system, comprising flexible computing and operating systems;</p>
            <p className='small bold black m-0 no-mobile'>A partnership approach to business, building partnerships with clients to anticipate
              needs and provide proactive solutions.</p>
            <p className='big bold black m-0 no-tablet no-desktop'>Highly skilled, professional staff who are constantly evolving to meet the current
              requirements within the sector;</p>
            <p className='big bold black m-0 no-tablet no-desktop'>A high-tech management system, comprising flexible computing and operating systems;</p>
            <p className='big bold black m-0 no-tablet no-desktop'>A partnership approach to business, building partnerships with clients to anticipate
              needs and provide proactive solutions.</p>
          </div>
        </div>
        <div className={ s.footer }>
          <ButtonDownload/>
        </div>
      </div>
    </div>
  )
}

export default Information
