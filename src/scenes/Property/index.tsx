import { useReactiveVar } from '@apollo/client'
import { appVar } from 'cache/vars'
import { Breadcrumbs, Search, CloudImage } from 'components'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import addDealTypeField from 'services/addDealTypeField'
import formatNumberWith from 'services/formatNumberWith'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'
import roundNumber from 'services/roundNumber'
import slugParser from 'services/slugParser'
import { ImageCarousel, GeneralInformation, UnitsList, ContactForm, RelatedProperties } from './components'
import s from './style.module.sass'
import cn from 'classnames'

import _, { startCase } from 'lodash'

import NoPropertyImage from 'assets/pictures/no-property-image.png'

interface Props {
  propertyList: AnyProperty[]
  relatedProperties?: AnyProperty[]
  isUnit?: boolean
}

const characterLimit = 600

const Property: React.FC<Props> = ({
  isUnit,
  propertyList,
  relatedProperties
}) => {
  const router = useRouter()

  const [descriptionHasShowMore, setDescriptionHasShowMore] = useState(false)
  const [descriptionShowMore, setDescriptionShowMore] = useState(false)
  const [isUnitInformation, setIsUnitInformation] = useState<string | null>(null)
  const [list, setList] = useState<AnyPropertyWithCustomFields>(groupPropertiesByGmavenKey(addDealTypeField(propertyList))[0])

  const item = isUnit ? propertyList.find(i => slugParser.parse('unit_slug' in i ? i.unit_slug! : i.property_slug!).unit_id === router.query.property?.[5])! : propertyList[0]

  const { algoliaStats } = useReactiveVar(appVar)
  const unitsCount = algoliaStats.facets.property_gmaven_key?.[item.property_gmaven_key || ""] || 0

  const itemImages = isUnit && 'unit_images' in item ?
    (item.unit_images || (item.best_image && [{
      image_id: 'image_id',
      image_path_url: item.best_image,
      type: 'image'
    }] ) || [])
  :
    (item.property_images || (item.best_image && [{
      image_id: 'image_id',
      image_path_url: item.best_image,
      type: 'image'
    }] ) || [])

  useEffect(() => {
    item.marketing?.property_marketing_description && item.marketing?.property_marketing_description.length > characterLimit ?
      setDescriptionHasShowMore(true)
    :
      setDescriptionHasShowMore(false)
  }, [item])

  useEffect(() => {
    const groupProperties = groupPropertiesByGmavenKey(addDealTypeField(propertyList))
    setList(groupProperties[groupProperties.length - 1])
  }, [propertyList])

  const scrollToPropertyContactForm = (web_ref: string) => {
    const boundingClientRect = document.getElementById('propertyContactForm')?.offsetTop
    boundingClientRect && window.scrollTo({ top: boundingClientRect, behavior: 'smooth' })
    setIsUnitInformation(web_ref)
  }

  const breadcrumbsMap = isUnit ?
    [`${ startCase(item.dealType) }`, item.unit_category! || item.property_category!, item.city!, item.suburb!, item.property_name!]
  :
    [`${ startCase(item.dealType) }`, item.property_category!, item.city!, item.suburb!, item.property_name!]

  return (
    <div className={ s.Property }>
      <div className={ s.layout }>
        <div className={ cn(s.section1) }>
          <div className={ s.wrapper }>
            <Breadcrumbs items={ breadcrumbsMap } property={ item }/>
          </div>
          <Search/>
        </div>
        <div className={ s.section2 }>
          <div className={ s.left }>
            <h2 className={ cn('small light uppercase m-0 cyan-4', s.dealType) }>{ startCase(item.dealType) }</h2>
            <p className="dark-2 big uppercase m-0">{ item.suburb }</p>
            <h2 className='small bold uppercase m-0 black'>{ item.property_name }</h2>
            <p className="bold uppercase black mt-05">{ item.street_address }</p>
            <div className={ cn(s.description, 'mv-1 pr-2 box-sizing no-mobile') }>
              <p className='regular m-0 black'>
                { descriptionHasShowMore ?
                  !descriptionShowMore ?
                    item.marketing?.property_marketing_description && item.marketing?.property_marketing_description.slice(0, characterLimit).trim() + '...' || "No description provided"
                  :
                    item.marketing?.property_marketing_description || "No description provided"
                :
                  item.marketing?.property_marketing_description || "No description provided"
                }
              </p>
              { descriptionHasShowMore &&
                <span className={ cn(s.showMore, 'regular underline cursor-pointer black') } onClick={ () => setDescriptionShowMore(!descriptionShowMore) }>
                  { descriptionShowMore ? "show less" : "show more" }
                </span>
              }
            </div>
            <div className={ cn(s.bottom, 'no-mobile') }>
              <h4 className='black bold m-0'>{ item.dealType === 'toLet' ? 'Gross Rental' : 'Price' }</h4>
              <h2 className='bold m-0 black'>
                { !list.gross_price ?
                  "POA"
                : item.dealType === 'toLet' ?
                  unitsCount > 1 && list.min_price !== list.max_price ?
                    `R${ formatNumberWith(' ',  roundNumber(list.min_price || 0)) } - ${ formatNumberWith(' ',  roundNumber(list.max_price || 0)) }${ item.dealType === 'toLet' ? ' per m²' : '' }`
                  :
                    `R${ formatNumberWith(" ", roundNumber(list.gross_price)) } per m²`
                :
                  `R${ formatNumberWith(" ", roundNumber(list.gross_price)) }`
                }
              </h2>
            </div>
          </div>
          <div className={ s.right }>
            <div className={ s.wrapper }>
              { itemImages.length ?
                <ImageCarousel
                  images={ itemImages }
                  video={ item.dealType === 'toLet' ? item.video : 'property_video' in item ? item.property_video : undefined }
                />
              :
                <CloudImage
                  className='covered'
                  src={ item.best_image || 'unit_images' in item && item.unit_images?.[0]?.image_path_url  || NoPropertyImage }
                  alt={ item.property_name }
                  responsive={{
                    desktopLarge: { w: '800', ar: '1' },
                    desktop: { w: '600', ar: '1' },
                    tablet: { w: '500', ar: '1' },
                    mobile: { w: '400', ar: '1' }
                  }}
                />
              }
            </div>
            <div className={ cn(s.description, 'mt-4 mb-2 box-sizing no-desktop no-tablet') }>
              <p className='regular m-0 black'>
                { descriptionHasShowMore ?
                  !descriptionShowMore ?
                    item.marketing?.property_marketing_description && item.marketing?.property_marketing_description.slice(0, characterLimit).trim() + '...' || "No description provided"
                  :
                    item.marketing?.property_marketing_description || "No description provided"
                :
                  item.marketing?.property_marketing_description || "No description provided"
                }
              </p>
              { descriptionHasShowMore &&
                <span className={ cn(s.showMore, 'regular underline cursor-pointer black') } onClick={ () => setDescriptionShowMore(!descriptionShowMore) }>
                  { descriptionShowMore ? "show less" : "show more" }
                </span>
              }
            </div>
            <div className={ cn(s.bottom, 'no-desktop no-tablet') }>
              <h4 className='black bold m-0'>{ item.dealType === 'toLet' ? 'Gross Rental' : 'Price' }</h4>
              <h2 className='bold m-0 black'>
                { !list.gross_price ?
                  "POA"
                : item.dealType === 'toLet' ?
                  unitsCount > 1 && list.min_price !== list.max_price ?
                    `R${ formatNumberWith(' ',  roundNumber(list.min_price || 0)) } - ${ formatNumberWith(' ',  roundNumber(list.max_price || 0)) }${ item.dealType === 'toLet' ? ' per m²' : '' }`
                  :
                    `R${ formatNumberWith(" ", roundNumber(list.gross_price)) } per m²`
                :
                  `R${ formatNumberWith(" ", roundNumber(list.gross_price)) }`
                }
              </h2>
            </div>
          </div>
          <div className={ cn(s.line, 'no-mobile') }/>
        </div>
        <div className={ s.section3 }>
          <GeneralInformation property={ list } noPropertyImage={ !itemImages.length } unitsCount={ unitsCount }/>
        </div>
        { item.dealType === 'toLet' &&
          <div className={ cn(s.section4, 'pt-2 pb-3 box-sizing') }>
            <UnitsList list={ propertyList } onClickToContactForm={ scrollToPropertyContactForm }/>
          </div>
        }
        <div className={ cn(s.section5) } id='propertyContactForm'>
          <ContactForm
            people={ isUnit && 'unit_responsibility' in item ? item.unit_responsibility : item.property_responsibility }
            dealType={ item.dealType }
            property={ item }
            web_ref={ isUnitInformation }
          />
        </div>
        <div className={ s.section6 }>
          { !!relatedProperties?.length &&
            <RelatedProperties list={ relatedProperties }/>
          }
        </div>
      </div>
    </div>
  )
}

export default Property

