import React from 'react'
import cn from 'classnames'
import formatNumberWith from 'services/formatNumberWith'
import roundNumber from 'services/roundNumber'

import s from './style.module.sass'
import Icon1 from 'assets/icons/property1.svg'
import Icon2 from 'assets/icons/property2.svg'
import Icon3 from 'assets/icons/property3.svg'
import Icon4 from 'assets/icons/property4.svg'
import Icon5 from 'assets/icons/property5.svg'
import { startCase } from 'lodash'

interface IProps {
  property: AnyPropertyWithCustomFields
  noPropertyImage: boolean
  unitsCount: number
}

const GeneralInformation: React.FC<IProps> = ({ property, noPropertyImage, unitsCount}) => {

  return (
    <div className={ cn(s.GeneralInformation) }>
      <div className={ cn(s.layout, { [s.noPropertyImage]: noPropertyImage }) }>
        <div className={ cn(s.item, s.blue) }>
          <div className={ s.icon }>
            <img src={ Icon1 } alt={ property.property_name } className="contained"/>
          </div>
          <div className={ cn(s.info, 'pr-6 box-sizing') }>
            <h4 className='bold white m-0 uppercase'>{ startCase(property.dealType) }</h4>
          </div>
        </div>
        <div className={ s.item }>
          <div className={ s.icon }>
            <img src={ Icon2 } alt={ property.property_name } className="contained"/>
          </div>
          <div className={ s.info }>
            <h4 className='bold black m-0'>SQM:</h4>
            <p className='big light m-0'>
              { property.min_gla !== property.max_gla ?
                `${ formatNumberWith(' ', property.min_gla || 0) }m² - ${ formatNumberWith(' ', property.max_gla || 0) }m²`
                :
                `${ formatNumberWith(' ', property.min_gla || 0) }m²`
              }
            </p>
          </div>
        </div>
        <div className={ s.item }>
          <div className={ s.icon }>
            <img src={ Icon3 } alt={ property.property_name } className="contained"/>
          </div>
          <div className={ s.info }>
            <h4 className='bold black m-0'>{ property.dealType === 'toLet' ? 'Gross Rental:' : 'Price:' }</h4>
            <p className='big light m-0'>
              { !property.gross_price ?
                "POA"
                : property.dealType === 'toLet' ?
                  unitsCount > 1 && property.min_price !== property.max_price ?
                    `R${ formatNumberWith(' ',  roundNumber(property.min_price || 0)) } - ${ formatNumberWith(' ',  roundNumber(property.max_price || 0)) }${ property.dealType === 'toLet' ? ' per m²' : '' }`
                    // `from R${ formatNumberWith(" ", roundNumber(item.gross_price)) } per m²`
                    :
                    `R${ formatNumberWith(" ", roundNumber(property.gross_price)) } per m²`
                  :
                  `R${ formatNumberWith(" ", roundNumber(property.gross_price)) }`
              }
            </p>
          </div>
        </div>
        <div className={ s.item }>
          <div className={ s.icon }>
            <img src={ property.dealType === 'toLet' ? Icon4 : Icon5 } alt={ property.property_name } className="contained"/>
          </div>
          <div className={ s.info }>
            <h4 className='bold black m-0'>{ property.dealType === 'toLet' ? 'ESC:' : 'Type:' }</h4>
            <p className='big light m-0'>
              { property.dealType === 'forSale'
                ?
                  'property_type' in property && startCase(property.property_type)
                :
                  'TBC'
                //   !property.min_esc && !property.max_esc
                // ?
                //   'TBC'
                // :
                //   property.min_esc === property.max_esc
                // ?
                //   `${ property.min_esc }%`
                // :
                //   `from ${ property.min_esc }%`
              }
            </p>
          </div>
        </div>
      </div>
      <div className={ s.layer }/>
    </div>
  )
}

export default GeneralInformation
