import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { pickBy } from 'lodash'
import { useForm } from 'hooks'
import { useForm as useFormspree } from '@formspree/react'
import formatPhoneNumber from 'services/formatPhoneNumber'
import ReCAPTCHA from 'react-google-recaptcha'

import { CloudImage, Button, Input, Textarea } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

import Logo from 'assets/pictures/logo.svg'

interface Props {
  people?: Responsibility[]
  property: AnyProperty
  dealType?: AnyProperty['dealType']
  web_ref?: string | null
}

interface Form {
  name: string
  contactNumber: string
  email: string
  message: string
}

const ContactForm: React.FC<Props> = ({ people, property, dealType, web_ref }) => {
  const router = useRouter()
  const [isMobile, setIsMobile] = useState(false)

  const getInitialMessage = () => {
    if (dealType === 'toLet' && web_ref) {
      return `Please send me more information on ${ property.property_name } in ${ property.suburb } with web reference: ${ web_ref }. Thank you.`
    }

    if (dealType === 'toLet') {
      return `Please send me more information on ${ property.property_name } in ${ property.suburb }. Thank you.`
    } else {
      return `Please send me more information on ${ property.property_name } for sale with web reference: ${ property.web_ref }. Thank you.`
    }
  }

  const [loading, setLoading] = useState(false)
  const [formSent, setFormSent] = useState(false)
  const [message, setMessage] = useState('')
  const [captcha, setCaptcha] = useState<string | null>(null)
  const [showNumber, setShowNumber] = useState<string[]>([])
  const [showEmail, setShowEmail] = useState<string[]>([])

  const list: Responsibility[] = people ? people.slice(0, 2) : []

  const handleCaptcha = (token: string | null) => {
    setCaptcha(token)
  }

  const [formspreeState, handleFormspreeSubmit] = useFormspree(process.env['NEXT_PUBLIC_FORMSPREE_PROPERTY_CONTACT_ID']!)

  useEffect(() => {
    if (formspreeState.succeeded) {
      setLoading(false)
      setFormSent(true)
      setMessage('Your message has been sent successfully.')
    } else if (formspreeState.errors.length) {
      setLoading(false)
      setMessage('Something went wrong, please try again later.')
    }
  }, [formspreeState])

  const handleSubmit = (values: Form, valid: boolean, errors: { [key in keyof Form]: string | null }) => {
    if (valid) {
      setLoading(true)
      handleFormspreeSubmit({
        property_id: property.objectID,
        name: values.name,
        contactNumber: values.contactNumber,
        email: values.email,
        message: values.message
      })
    } else {
      const errorsList = Object.values(pickBy(errors))
      const firstError = errorsList.includes('Field is required') ? 'Field is required' : errorsList[0]

      if (firstError) {
        const text = firstError === 'Field is required' ? "Please fill the required fields." : firstError
        setMessage(text)
      }
    }
  }

  const { values, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true, validator: 'min2' },
      contactNumber: { initialValue: '', required: true, validator: 'phone' },
      email: { initialValue: '', required: true, validator: 'email' },
      message: { initialValue: '' }
    }, handleSubmit
  })

  useEffect(() => {
    change({ name: 'message', value: getInitialMessage() })
  }, [router.asPath, web_ref])

  const handleShowNumber = (number: string) => {
    if (showNumber.includes(number)) {
      setShowNumber(showNumber.filter(i => i !== number))
    } else {
      setShowNumber([...showNumber, number])
    }
  }

  const handleShowEmail = (email: string) => {
    if (showEmail.includes(email)) {
      setShowEmail(showEmail.filter(i => i !== email))
    } else {
      setShowEmail([...showEmail, email])
    }
  }

  useEffect(() => {
    setIsMobile(window.innerWidth <= 767)
  }, [])



  return (
    <div className={ cn(s.ContactForm) }>
      <h3 className='big bold uppercase dark-2 m-0 mb-3'>CONTACT</h3>
      <div className={ s.layout }>
        <div className={ s.left }>
          <div className={ s.fields }>
            <Input secondary name="name" placeholder="Name" disabled={ formSent }
                   onChange={ change } required grayBg className='mb-1' value={ values.name }/>
            <Input secondary name="contactNumber" grayBg placeholder="Contact Number" disabled={ formSent }
                   onChange={ change } required className='mb-1' value={ values.contactNumber }/>
            <Input secondary name="email" grayBg placeholder="Email Address" disabled={ formSent }
                   onChange={ change } required value={ values.email }/>
            <Textarea grayBg secondary name="message" rows={ isMobile ? 5 : 11 } onChange={ change } value={ values.message } disabled={ formSent }/>
            <p className={ cn({ 'no-pointer-events no-select opacity-0': !message }) }>{ message || "_" }</p>
          </div>
          <div className={ cn(s.footer) }>
            <div className={ cn(s.captcha, 'mb-05') }>
              <ReCAPTCHA
                sitekey={ process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY || '' }
                onChange={ handleCaptcha }
              />
            </div>
          </div>
        </div>
        { !!list.length ? (
          <div className={ cn(s.right, 'box-sizing') }>
            { list.map((i, num) =>
              <div key={ `${ i.gmaven_contact_key }-${ num }` } className={ cn(s.item) }>
                <div className={ cn(s.avatar, 'overflow-hidden') }>
                  <CloudImage
                    className="covered"
                    src={ i.image }
                    alt={ i.name }
                    responsive={{
                      desktopLarge: { w: '300', ar: '1' },
                      desktop: { w: '200', ar: '1' },
                      tablet: { w: '180', ar: '1' },
                      mobile: { w: '140', ar: '1' }
                    }}
                  />
                  <div className={ s.border }/>
                </div>
                <div className={ cn(s.info, 'text-center mb-1') }>
                  <h4 className='bold black'>{ i.name }</h4>
                  { i.cell_number &&
                    <>
                      <p className='cyan-4 cursor-pointer m-0' onClick={ () => handleShowNumber(i.cell_number || '') }>{ showNumber.includes(i.cell_number) ? 'Hide number' : 'Show number' }</p>
                      { showNumber.includes(i.cell_number) &&
                        <p className='light m-0'>{ formatPhoneNumber(`+${ i.cell_number! }`) }</p>
                      }
                    </>
                  }
                  { i.email &&
                    <>
                      <p className='cyan-4 cursor-pointer m-0' onClick={ () => handleShowEmail(i.email || '') }>{ showEmail.includes(i.email) ? 'Hide email' : 'Show email' }</p>
                      { showEmail.includes(i.email) &&
                        <a className='light m-0' href={ `mailto:${i.email}` } target='_blank' rel='noopener noreferrer'>{ i.email }</a>
                      }
                    </>
                  }
                </div>
              </div>
            )}
          </div>
        ) : (
          <div className={ cn(s.right, 'box-sizing mb-1') }>
            <div className={ cn(s.noBrokerContainer) }>
              <img className={ cn(s.logo, 'contained') } src={ Logo } alt="Broll logo"/>
              <div className='mt-05'>
                <p className='mv-05'><span className='light lightRed'>Tel</span><span className='light'> +27 (0)11 463 0518</span></p>
                <p className='mv-05'>
                  <span className='light lightRed'>Email</span>
                  <a className='light' href='mailto:admin@prominentprops.co.za' target='_blank' rel='noopener noreferrer'> admin@prominentprops.co.za</a>
                </p>
              </div>
            </div>
          </div>
        ) }
      </div>
      <div className={ cn(s.footer) }>
        <Button onClick={ submit } loading={ loading } disabled={ !captcha || formSent }>Send</Button>
        <div className={ s.line }/>
      </div>
    </div>
  )
}

export default ContactForm
