import React, { useEffect, useRef, useState } from 'react'
import ReactDOM from 'react-dom'
import ReactImageVideoLightBox from 'react-image-video-lightbox'

import Slider, { Settings } from 'react-slick'
import { CloudImage, Icon } from 'components'
import FullSizeIcon from 'assets/icons/fullSize.svg'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrow.svg'

const settings: Settings = {
  className: "slickSlider",
  centerMode: true,
  centerPadding: "0",
  slidesToShow: 1,
  speed: 0
}

interface Props {
  images: Image[]
  video?: string
}

type CarouselItem = {
  url: string
  type: 'photo' | 'video'
  altTag?: string
  title?: string
}

const Portal: React.FC = ({ children }) =>
  ReactDOM.createPortal(
    children,
    document.getElementById('Modal') as Element,
  )

const ImageCarousel: React.FC<Props> = ({ images, video }) => {
  const items: CarouselItem[] = images.map((image) => ({
    url: image.image_path_url || '',
    type: 'photo',
    altTag: image.image_id
  }))
  if (video) items.push({ url: `https://www.youtube.com/embed/${ video }`, type: 'video', title: 'Property video' })

  const sliderRef = useRef<Slider>(null)
  const [isZoomed, setIsZoomed] = useState(false)
  const [currentIndex, setCurrentIndex] = useState(0)
  const [isVideo, setIsVideo] = useState(false)

  const handleChange = (type: 'prev' | 'next') => {
    if (sliderRef.current) {
      if (type === 'prev') {
        setCurrentIndex((currentIndex + items.length - 1) % items.length)
        sliderRef.current.slickPrev()
      } else {
        setCurrentIndex((currentIndex + 1) % items.length)
        sliderRef.current.slickNext()
      }
    }
  }

  const handleClose = () => {
    setIsZoomed(false)
  }

  useEffect(() => {
    if (items[currentIndex].type === 'video') {
      setIsVideo(true)
    }
    if (items[currentIndex].type === 'photo' && isVideo) {
      setIsVideo(false)
    }
  }, [currentIndex])

  return (
    <div className={ cn(s.ImageCarousel) }>
      <div className={ s.layout }>
        <Slider ref={ sliderRef } { ...settings }>
          { items.map((item, num) => item.type === 'photo'  ? (
            <div key={ `${ item.altTag }-${ num }` } className={ cn(s.item) }>
              <div className={ s.image }>
                <CloudImage
                  className="covered"
                  src={ item.url }
                  alt={ item.altTag }
                  responsive={{
                    desktopLarge: { w: '800', ar: '1' },
                    desktop: { w: '600', ar: '1' },
                    tablet: { w: '500', ar: '1' },
                    mobile: { w: '400', ar: '1' }
                  }}
                />
                <div className={ s.gradient }/>
              </div>
            </div>
            ) : (
            <div key={ video } className={ cn(s.item) }>
              <div className={ s.image } >
                <iframe
                  className={ cn(s.iframe) }
                  src={ `https://www.youtube.com/embed/${isVideo && items[currentIndex].type === 'photo' ? 'https://www.youtube.com/' : video}` }
                  title='YouTube video player' frameBorder='0'
                  allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                  allowFullScreen
                />
                {/*<div className={ s.gradient }/>*/}
              </div>
            </div>
          )) }
        </Slider>
        { isZoomed &&
          <Portal>
              <div className={ s.lightBoxContainer }>
                <ReactImageVideoLightBox
                  data={ items }
                  startIndex={ currentIndex }
                  onCloseCallback={ handleClose }
                />
              </div>
          </Portal>
        }
        <div className={ cn(s.sliderControls, s.left, "cursor-pointer no-select") } onClick={ () => handleChange('prev') }>
          <Icon src={ ArrowIcon } rotate={ 270 } size='s'/>
        </div>
        <div className={ cn(s.sliderControls, s.right, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
          <Icon src={ ArrowIcon } rotate={ 90 } size='s'/>
        </div>
        { items[currentIndex].type === 'photo' &&
          <div className={cn(s.sliderControls, s.bottom, "cursor-pointer no-select")} onClick={() => setIsZoomed(true)}>
              <Icon src={ FullSizeIcon } size='l'/>
          </div>
        }
      </div>
    </div>
  )
}

export default ImageCarousel
