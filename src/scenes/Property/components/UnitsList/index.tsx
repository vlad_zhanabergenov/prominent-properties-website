import React, { useState } from 'react'
import formatNumberWith from 'services/formatNumberWith'

import { CloudImage, Button } from 'components'

import cn from 'classnames'
import NoPropertyImage from 'assets/pictures/no-property-image.png'
import EmailIcon from 'assets/icons/email.png'
import roundNumber from 'services/roundNumber'

import s from './style.module.sass'

interface Props {
  list: AnyProperty[]
  onClickToContactForm: (web_ref: string) => void
}

const maxUnitsToShow = 6

const UnitsList: React.FC<Props> = ({ list, onClickToContactForm }) => {
  const [limit, setLimit] = useState(true)
  const units = list.slice(0, limit ? maxUnitsToShow : list.length)

  return (
    <div className={ cn(s.UnitsList) }>
      <div className={ s.layout }>
        <h3 className='big bold uppercase dark-2 m-0'>Units</h3>
        <div className={ s.units }>
          { units.map((item, num) => <div key={ `${ item.objectID }-${ num }` } className={ cn(s.unit, 'pv-1 box-sizing') }>
              <div className={ cn(s.wrapper) }>
                <div className={ s.img }>
                  { ('unit_images' in item && item.unit_images?.length) ?
                      <CloudImage
                        className='covered'
                        src={ item.unit_images[0].image_path_url || NoPropertyImage }
                        alt={ `${ item.property_name }, ${ item.marketing?.unit_marketing_heading }` }
                        responsive={{
                          desktopLarge: { w: '300', ar: '1.1' },
                          desktop: { w: '200', ar: '1.1' },
                          tablet: { w: '180', ar: '1.1' },
                          mobile: { w: '120', ar: '1.1' }
                        }}
                      />
                    :
                      <CloudImage
                        className='covered'
                        src={ item.best_image || NoPropertyImage }
                        alt={ `${ item.property_name }, ${ item.marketing?.unit_marketing_heading }` }
                        responsive={{
                          desktopLarge: { w: '300', ar: '1.1' },
                          desktop: { w: '200', ar: '1.1' },
                          tablet: { w: '180', ar: '1.1' },
                          mobile: { w: '120', ar: '1.1' }
                        }}
                      />
                  }
                  <div className={ s.layer }/>
                </div>
                <div className={ cn(s.data) }>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <p className='black bold big limit-string-1 mb-05'>Unit ID:</p>
                    <h3 className='black big regular limit-string-1 m-0 no-mobile'>{ item.unit_id }</h3>
                    <h4 className='black small regular limit-string-1 m-0 no-tablet no-desktop'>{ item.unit_id }</h4>
                  </div>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <p className='black bold big limit-string-1 mb-05'>Price:</p>
                    <h3 className='black big regular limit-string-1 m-0 no-mobile'>
                      { !item.gross_price ?
                          "POA"
                        :
                          `R${ formatNumberWith(" ", roundNumber(item.gross_price)) }/m²`
                      }
                    </h3>
                    <h4 className='black small regular limit-string-1 m-0 no-desktop no-tablet'>
                      { !item.gross_price ?
                        "POA"
                        :
                        `R${ formatNumberWith(" ", roundNumber(item.gross_price)) }/m²`
                      }
                    </h4>
                  </div>
                  <div className={ cn(s.col, 'pr-05 box-sizing') }>
                    <p className='black bold big limit-string-1 mb-05'>GLA:</p>
                    <h3 className='black big regular limit-string-1 m-0 no-mobile'>
                      { `${ formatNumberWith(' ', item.max_gla || 0) }m²` }
                    </h3>
                    <h4 className='black small regular limit-string-1 m-0 no-desktop no-tablet'>
                      { `${ formatNumberWith(' ', item.max_gla || 0) }m²` }
                    </h4>
                  </div>
                </div>
                <div className={ s.actions } onClick={ () => onClickToContactForm(item.web_ref || '') }>
                  <Button full boldFont className='no-mobile'>Email</Button>
                  <div className={ cn(s.icon, 'no-desktop no-tablet')}>
                    <img src={ EmailIcon } alt={ item.property_name } className='contained'/>
                  </div>
                </div>
              </div>
              <div className={ s.line }/>
            </div>
          )}
        </div>
        { (limit && list.length > maxUnitsToShow) &&
          <div className={ cn(s.loadMore, 'pt-2 pb-1 box-sizing') }>
            <h4 className="bold uppercase cursor-pointer text-center m-0" onClick={ () => setLimit(false) }>
             + load more
            </h4>
          </div>
        }
      </div>
    </div>
  )
}

export default UnitsList
