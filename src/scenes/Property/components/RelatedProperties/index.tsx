import React from 'react'
import groupPropertiesByGmavenKey from 'services/groupPropertiesByGmavenKey'

import { RelatedPropertiesCarousel } from './components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  list: AnyProperty[]
}

const RelatedProperties: React.FC<Props> = ({ list }) => {
  const items = groupPropertiesByGmavenKey(list)

  return (
    <div className={ cn(s.RelatedProperties) }>
      <div className={ cn(s.layout) }>
        <div className={ cn(s.heading, 'mb-2 ml-05') }>
          <h3 className='big bold uppercase dark-2 m-0'>Related</h3>
        </div>
        <div className={ cn(s.carousel) }>
          <RelatedPropertiesCarousel list={ items }/>
        </div>
      </div>
    </div>
  )
}

export default RelatedProperties
