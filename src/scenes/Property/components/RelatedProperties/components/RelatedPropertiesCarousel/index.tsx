import React, { useRef } from 'react'
import Slider, { Settings } from "react-slick"

import { Icon, PropertyCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrowBoldCyan.svg'

const settings: Settings = {
  className: "slickSlider",
  centerMode: true,
  infinite: true,
  centerPadding: "0",
  slidesToShow: 3,
  speed: 500,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        centerMode: false,
        variableWidth: true
      }
    }
  ]
}

interface Props {
  list: AnyProperty[]
}

const RelatedPropertiesCarousel: React.FC<Props> = ({ list }) => {
  const sliderRefDesktop = useRef<Slider>(null)
  const sliderRefMobile = useRef<Slider>(null)

  const handleChange = (type: 'prev' | 'next') => {
    if (sliderRefDesktop.current) {
      if (type === 'prev') {
        sliderRefDesktop.current.slickPrev()
      } else {
        sliderRefDesktop.current.slickNext()
      }
    }
    if (sliderRefMobile.current) {
      if (type === 'prev') {
        sliderRefMobile.current.slickPrev()
      } else {
        sliderRefMobile.current.slickNext()
      }
    }
  }

  if (!list.length) {
    return null
  }

  const mapList = list.map(item =>
    <div key={ `${ item.objectID }-${ item.property_name }` } className={ cn(s.item, 'ph-05 box-sizing') }>
      <PropertyCard item={ item }/>
    </div>
  )

  return (
    <div className={ cn(s.RelatedPropertiesCarousel) }>
      <div className={ cn(s.layout, 'no-tablet no-mobile') }>
        { list.length <= 3 ?
            <div className={ s.list }>
              { mapList }
            </div>
          :
            <>
              <Slider key="sliderDesktop" ref={ sliderRefDesktop } { ...settings }>
                { mapList }
              </Slider>
              <div className={ cn(s.sliderControls, s.left, "cursor-pointer no-select") } onClick={ () => handleChange('prev') }>
                <Icon src={ ArrowIcon } rotate={ 90 }/>
              </div>
              <div className={ cn(s.sliderControls, s.right, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
                <Icon src={ ArrowIcon } rotate={ 270 }/>
              </div>
            </>
        }
      </div>
      <div className={ cn(s.layout, 'no-desktop') }>
        <Slider key="sliderMobile" ref={ sliderRefMobile } { ...settings }>
          { mapList }
        </Slider>
      </div>
    </div>
  )
}

export default RelatedPropertiesCarousel
