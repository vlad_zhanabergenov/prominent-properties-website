import React from 'react'
import { ContactForm } from './components'

import s from './style.module.sass'
import cn from 'classnames'

const Contact = () => {
  return (
    <div className={ s.Contact }>
      <div className={ s.layout }>
        <div className={ s.heading }>
          <h3 className='big bold uppercase dark-2 m-0'>Contact us</h3>
        </div>
        <div className={ s.form }>
          <div className={ cn(s.section1, 'mb-3 ml-1') }>
            <h3 className='small light m-0 no-mobile'>Please fill in your contact information below and a consultant will get <br/> back to you. Alternatively, you can get hold of us directly at:</h3>
            <h3 className='small light m-0 no-desktop no-tablet'>Please fill in your contact information below and a consultant will get <br/> back to you. Alternatively, you can get hold of us directly at:</h3>
            <h3 className='small bold m-0'>Promops CC T/A PROMINENT PROPERTIES</h3>
          </div>
          <div className={ s.section2 }>
            <ContactForm/>
          </div>
        </div>
        <div className={ s.footer }>
          <div className={ s.left }>
            <h3 className='small cyan-4 medium m-0 mb-1'>Physical Address:</h3>
            <div className={ s.info }>
              <h3 className='small black light m-0 ml-2'>La Rocca Block C, Ground Floor</h3>
              <h3 className='small black light m-0 ml-2'>321 Main Road</h3>
              <h3 className='small black light m-0 ml-2'>Bryanston 2037</h3>
              <div className={ s.wrapper }>
                <h3 className='small light m-0 cyan-4'>T</h3>
                <h3 className='small black light m-0'>+27 (0)11 463 0518</h3>
              </div>
              <div className={ s.wrapper }>
                <h3 className='small light m-0 cyan-4'>E</h3>
                <h3 className='small black light m-0'>admin@prominentprops.co.za</h3>
              </div>
            </div>
          </div>
          <div className={ s.right }>
            <h3 className='small cyan-4 medium m-0 mb-1'>Postal Address:</h3>
            <div className={ s.info }>
              <h3 className='small black light m-0 ml-2'>PO Box 653 229</h3>
              <h3 className='small black light m-0 ml-2'>Benmore</h3>
              <h3 className='small black light m-0 ml-2'>2010</h3>
              <div className={ s.wrapper }>
                <h3 className='small light m-0 cyan-4'>F</h3>
                <h3 className='small black light m-0'>+27 (0)86 505 0850</h3>
              </div>
              <div className={ s.wrapper }>
                <h3 className='small light m-0 cyan-4'>W</h3>
                <h3 className='small black light m-0'>www.prominentprops.co.za</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact
