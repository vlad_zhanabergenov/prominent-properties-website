import React, { useEffect, useState } from 'react'
import { pickBy } from 'lodash'
import { useForm } from 'hooks'
import { useForm as useFormspree } from '@formspree/react'
import ReCAPTCHA from 'react-google-recaptcha'

import { Select, Button, Input, Textarea } from 'components'

import { GoogleMap } from '../index'

import cn from 'classnames'
import s from './style.module.sass'

interface Form {
  name: string
  contactNumber: string
  email: string
  aboutUs: string
  message: string
}

const list = ['Google', 'Advertisment', 'Word of mouth', 'Other']

const ContactForm: React.FC = () => {

  const [loading, setLoading] = useState(false)
  const [formSent, setFormSent] = useState(false)
  const [message, setMessage] = useState('')
  const [captcha, setCaptcha] = useState<string | null>(null)

  const handleCaptcha = (token: string | null) => {
    setCaptcha(token)
  }

  const [formspreeState, handleFormspreeSubmit] = useFormspree(process.env['NEXT_PUBLIC_FORMSPREE_PROPERTY_CONTACT_ID']!)

  useEffect(() => {
    if (formspreeState.succeeded) {
      setLoading(false)
      setFormSent(true)
      setMessage('Your message has been sent successfully.')
    } else if (formspreeState.errors.length) {
      setLoading(false)
      setMessage('Something went wrong, please try again later.')
    }
  }, [formspreeState])

  const handleSubmit = (values: Form, valid: boolean, errors: { [key in keyof Form]: string | null }) => {
    if (valid) {
      setLoading(true)
      handleFormspreeSubmit({
        name: values.name,
        contactNumber: values.contactNumber,
        email: values.email,
        aboutUs: values.aboutUs,
        message: values.message
      })
    } else {
      const errorsList = Object.values(pickBy(errors))
      const firstError = errorsList.includes('Field is required') ? 'Field is required' : errorsList[0]

      if (firstError) {
        const text = firstError === 'Field is required' ? "Please fill the required fields." : firstError
        setMessage(text)
      }
    }
  }

  const { values, change, submit } = useForm<Form>({
    fields: {
      name: { initialValue: '', required: true, validator: 'min2' },
      contactNumber: { initialValue: '', required: true, validator: 'phone' },
      email: { initialValue: '', required: true, validator: 'email' },
      aboutUs: { initialValue: 'Google', required: true },
      message: { initialValue: '' }
    }, handleSubmit
  })


  return (
    <div className={ cn(s.ContactForm) }>
      <div className={ s.layout }>
        <div className={ s.wrapper }>
          <div className={ s.left }>
            <div className={ s.fields }>
              <Input secondary name="name" placeholder="Name" disabled={ formSent }
                     onChange={ change } required value={ values.name }/>
              <Input secondary name="contactNumber" placeholder="Contact Number" disabled={ formSent }
                     onChange={ change } required value={ values.contactNumber }/>
              <Input secondary name="email" placeholder="Email Address" disabled={ formSent }
                     onChange={ change } required value={ values.email }/>
              <p className='mv-1 ml-1'>Where did you hear about us <span className='red'>*</span></p>
              <Select name="aboutUs" options={ list } value={ values.aboutUs } onChange={ change } secondary/>
              <Textarea secondary name="message" rows={ 7 } placeholder='Message' onChange={ change } value={ values.message } disabled={ formSent }/>
              <p className={ cn( 'red', { 'no-pointer-events no-select opacity-0': !message }) }>{ message || "_" }</p>
            </div>
          </div>
          <div className={ s.right }>
            <GoogleMap latitude={ -26.0665142 } longitude={ 28.0159593 }/>
          </div>
        </div>
        <div className={ cn(s.footer) }>
          <div className={ cn(s.captcha, 'mb-1') }>
            <ReCAPTCHA
              sitekey={ process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY || '' }
              onChange={ handleCaptcha }
            />
          </div>
          <div className={ s.send }>
            <Button onClick={ submit } loading={ loading } disabled={ !captcha || formSent }>Send</Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactForm
