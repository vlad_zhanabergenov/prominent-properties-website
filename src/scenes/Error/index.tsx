import s from './style.module.sass'

const Error = () => {
  return (
    <div className={ s.Error }>
      <h2 className="white mt-0 mb-5">Page not found</h2>
    </div>
  )
}

export default Error
