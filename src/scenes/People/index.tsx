import React from 'react'
import { Card } from './components'
import s from './style.module.sass'

interface Props {
  personList: PersonItem[]
}

const People: React.FC<Props> = ({ personList }) => {

  return (
    <div className={ s.People }>
      <div className={ s.layout }>
        <h2 className="black mt-0 mb-3 fontSecondary uppercase"><span className='cyan-4 fontSecondary'>Meet our People,</span><br/> and find out what makes us tick.</h2>
        <div className={ s.list }>
          { personList.map(i =>
              <Card item={ i } key={ i.id }/>
          )}
        </div>
      </div>
    </div>
  )
}

export default People
