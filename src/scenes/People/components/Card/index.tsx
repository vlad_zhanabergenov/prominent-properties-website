import React, { useState } from 'react'
import { CloudImage, Icon } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import LinkedInIcon from 'assets/icons/linkedinBlack.svg'
import TwitterIcon from 'assets/icons/twitterBlack.svg'

interface Props {
  item: PersonItem
}

const Card: React.FC<Props> = ({ item }) => {
  const [active, setActive] = useState(false)

  return (
    <div className={ cn(s.Card, { [s.active]: active }) }>
      <div className={ s.layout }>
        <div className={ cn(s.front, { [s.even]: Number(item.id) % 2 === 0 }) } onClick={ () => setActive(!active) }>
          <div className={ s.image }>
            <CloudImage
              className="covered"
              src={ item.image }
              alt={ item.shortBio }
              responsive={{
                desktop: { w: '500', ar: '1.5' },
                tablet: { w: '400', ar: '1.5' },
                mobile: { w: '300', ar: '1.2' }
              }}
            />
          </div>
          <div className={ s.info }>
            <h3 className='m-0 black semiBig medium'>{ item.name.split(' ')[0] }</h3>
            <h3 className='m-0 black semiBig medium'>{ item.name.split(' ')[1] }</h3>
            <p className=' m-0 mt-1 italic light black'>{ item.title }</p>
          </div>
          <div className={ s.footer }>
            <h3 className='m-0 light semiBig black'>+</h3>
            <p className='m-0 light italic black'>- VIEW -</p>
            <h3 className='m-0 light semiBig black'>+</h3>
          </div>
        </div>
        <div className={ s.back }>
          <div className={ s.section1 }>
            <h3 className='m-0 black semiBig medium'>{ item.name.split(' ')[0] }</h3>
            <h3 className='m-0 black semiBig medium'>{ item.name.split(' ')[1] }</h3>
            <p className=' m-0 mt-1 light black'>{ item.title }</p>
            <p className=' m-0 mt-3 light black'>{ item.shortBio }</p>
          </div>
          <div className={ s.section2 }>
            <a href={ item.linkedIn } target="_blank" className='mr-05'>
              <Icon src={ LinkedInIcon } size='xl'/>
            </a>
            <a href={ item.twitter } target="_blank" className='ml-05'>
              <Icon src={ TwitterIcon } size='xl'/>
            </a>
          </div>
          <div className={ s.section3 }>
            <p className=' m-0 light black'>T +{ item.phone }</p>
            <p className=' m-0 light black'>E { item.email }</p>
          </div>
          <div className={ s.footer }>
            <h3 className='m-0 light semiBig black'>+</h3>
            <p className='m-0 light italic black'>- BACK -</p>
            <h3 className='m-0 light semiBig black'>+</h3>
          </div>
          <div className={ s.layer } onClick={ () => setActive(!active) }/>
        </div>
      </div>
    </div>
  )
}

export default Card
