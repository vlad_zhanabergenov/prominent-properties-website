import { Search } from 'components'
import React from 'react'
import { ImageCarousel, FeaturedProperties, OurServices } from './components'

import cn from 'classnames'
import s from "./style.module.sass"

interface Props {
  heroCarouselList: HeroCarouselItem[]
  featuredOfferingsList: AnyProperty[]
}

const Home: React.FC<Props> = ({
 featuredOfferingsList,
 heroCarouselList
}) => {
  return (
    <div className={ s.Home }>
      <div className={ cn(s.section1, 'pt-6') }>
        <ImageCarousel list={ heroCarouselList } />
      </div>
      <div className={ s.section2 }>
        <Search/>
      </div>
      <div className={ cn(s.section3, 'pb-3') }>
        <FeaturedProperties list={ featuredOfferingsList.slice(0, 3) }/>
      </div>
      <div className={ cn(s.section4) }>
        <OurServices/>
      </div>
    </div>
  )
}

export default Home
