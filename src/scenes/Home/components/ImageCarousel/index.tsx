import React, { useRef } from 'react'
import Slider, { Settings } from "react-slick"

import { Icon, CloudImage } from 'components'

import cn from 'classnames'
import s from './style.module.sass'
import ArrowIcon from 'assets/icons/arrowBlack.svg'

const settings: Settings = {
  className: "slickSlider",
  infinite: true,
  centerPadding: "0",
  speed: 500,
  variableWidth: true,
  swipe: false,
  autoplay: true,
  autoplaySpeed: 4000,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        centerPadding: "0",
        vertical: true,
        slidesToShow: 1,
        variableWidth: false
      }
    }
  ]
}

interface Props {
  list: HeroCarouselItem[]
}

const ImageCarousel: React.FC<Props> = ({ list }) => {
  const sliderRef = useRef<Slider>(null)

  const handleChange = (type: 'prev' | 'next') => {
    if (sliderRef.current) {
      if (type === 'prev') {
        sliderRef.current.slickPrev()
      } else {
        sliderRef.current.slickNext()
      }
    }
  }

  if (!list.length) {
    return null
  }


  return (
    <div className={ cn(s.ImageCarousel) }>
      <div className={ s.layout }>
        <Slider ref={ sliderRef } { ...settings }>
          { list.map(item =>
            <div key={ item.id } className={ cn(s.item) }>
              <div className={ s.image }>
                <CloudImage
                  responsive={{
                    desktopLarge: { w: '1300' },
                    desktop: { w: '1000' },
                    tablet: { w: '700' },
                    mobile: { w: '400' }
                  }}
                  className='covered no-mobile'
                  src={ item.image }
                  alt={ item.title }
                />
                <CloudImage
                  responsive={{
                    desktopLarge: { w: '1300' },
                    desktop: { w: '1000' },
                    tablet: { w: '700' },
                    mobile: { w: '400' }
                  }}
                  className='covered no-desktop no-tablet'
                  src={ item.mobileImage }
                  alt={ item.title }
                />
              </div>
              {/*<h3 className='big white regular m-0 text-right lhNormal'>{ item.title }</h3>*/}
            </div>
          )}
        </Slider>
        <div className={ s.line }/>
        <div className={ cn(s.sliderControls, "cursor-pointer no-select") } onClick={ () => handleChange('next') }>
          <Icon src={ ArrowIcon } size='xl' className='no-mobile'/>
          <Icon src={ ArrowIcon } size='l' rotate={ 90 } className='no-desktop no-tablet'/>
        </div>
      </div>
    </div>
  )
}

export default ImageCarousel
