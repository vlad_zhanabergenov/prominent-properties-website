import React from 'react'

import { FeaturedOfferingsCarousel } from './components'

import cn from 'classnames'
import s from './style.module.sass'

interface Props {
  list: AnyProperty[]
}

const OurServices: React.FC<Props> = ({ list }) => {
  return (
    <div className={ cn(s.FeaturedOfferings) }>
      <div className={ cn(s.layout, "overflow-hidden") }>
        <div className={ cn(s.heading, 'mb-3') }>
          <h3 className="medium semiBig dark m-0">FEATURED PROPERTIES</h3>
        </div>
        <div className={ cn(s.carousel, "overflow-hidden") }>
          <FeaturedOfferingsCarousel list={ list }/>
        </div>
      </div>
    </div>
  )
}

export default OurServices
