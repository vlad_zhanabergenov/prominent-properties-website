import React, { useRef } from 'react'
import Slider, { Settings } from "react-slick"

import { PropertyCard } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

const settings: Settings = {
  className: "slickSlider",
  swipe: false,
  infinite: true,
  centerPadding: "0",
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        variableWidth: true,
        speed: 500,
        swipe: true,
      }
    }
  ]
}

interface Props {
  list: AnyPropertyWithCustomFields[]
}

const FeaturedOfferingsCarousel: React.FC<Props> = ({ list }) => {
  const sliderRef = useRef<Slider>(null)

  if (!list.length) {
    return null
  }

  return (
    <div className={ cn(s.FeaturedOfferingsCarousel) }>
      <div className={ s.layout }>
        <Slider ref={ sliderRef } { ...settings }>
          { list.map(item =>
            <div key={ `${ item.objectID }-${ item.property_name }` } className={ cn(s.item, 'ph-05 box-sizing') }>
              <PropertyCard item={ item } />
            </div>
          )}
        </Slider>
      </div>
    </div>
  )
}

export default FeaturedOfferingsCarousel
