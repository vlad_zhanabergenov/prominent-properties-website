import React, { useState } from 'react'

import { ButtonDownload } from 'components'

import cn from 'classnames'
import s from './style.module.sass'

import Icon1 from 'assets/icons/service1.svg'
import Icon2 from 'assets/icons/service2.svg'
import Icon3 from 'assets/icons/service3.svg'
import Icon4 from 'assets/icons/service4.svg'
import Icon5 from 'assets/icons/service5.svg'
import Icon6 from 'assets/icons/service6.svg'
import Icon1Active from 'assets/icons/service1Active.svg'
import Icon2Active from 'assets/icons/service2Active.svg'
import Icon3Active from 'assets/icons/service3Active.svg'
import Icon4Active from 'assets/icons/service4Active.svg'
import Icon5Active from 'assets/icons/service5Active.svg'
import Icon6Active from 'assets/icons/service6Active.svg'

const data = [
  { id: 0, icon: Icon1, activeIcon: Icon1Active, title: 'Rent collection<br/> and credit control', heading: 'Rent collection and credit control', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' },
  { id: 1, icon: Icon2, activeIcon: Icon2Active, title: 'Leasing<br/> management and tenant liaison', heading: 'Leasing management', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' },
  { id: 2, icon: Icon3, activeIcon: Icon3Active, title: 'Account<br/> payments', heading: 'Account payments', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' },
  { id: 3, icon: Icon4, activeIcon: Icon4Active, title: 'Reporting', heading: 'Reporting', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' },
  { id: 4, icon: Icon5, activeIcon: Icon5Active, title: 'Technical management', heading: 'Technical management', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' },
  { id: 5, icon: Icon6, activeIcon: Icon6Active, title: 'Budget<br/> formulation and control', heading: 'Budget formulation and control', description: 'The appearance and layout of each store is critical to the overall centre. Prominent Properties and onsite retail management work closely with tenants, centre architects and other professionals regarding shop appearance. Merchandising, window displays, stock levels, product variety appeal and pricing are also regularly discussed with tenants.' }
]


const OurServices: React.FC = () => {
  const [active, setActive] = useState(0)

  return (
    <div className={ s.OurServices }>
      <div className={ s.heading }>
        <h2 className="medium small dark m-0 mb-3">OUR SERVICES</h2>
      </div>
      <div className={ s.layout }>
        <div className={ cn(s.top, 'no-mobile') }>
          { data.map(item =>
            <div key={ item.id } className={ cn(s.item, 'cursor-pointer', { [s.active]: active === item.id }) } onClick={ () => setActive(item.id) }>
              <div className={ s.icon }>
                <img src={ active === item.id ? item.activeIcon : item.icon } alt={ item.title } className='contained'/>
              </div>
              <h3 className='small light m-0' dangerouslySetInnerHTML={{ __html: item.title }}/>
            </div>
          )}
        </div>
        <div className={ cn(s.topTabs, 'no-desktop no-tablet') }>
          { data.slice(0, 3).map(item =>
            <div key={ item.id } className={ cn(s.item, 'cursor-pointer', { [s.active]: active === item.id }) } onClick={ () => setActive(item.id) }>
              <div className={ s.icon }>
                <img src={ active === item.id ? item.activeIcon : item.icon } alt={ item.title } className='contained'/>
              </div>
              <p className='big light m-0' dangerouslySetInnerHTML={{ __html: item.title }}/>
            </div>
          )}
        </div>
        <div className={ cn(s.bottom, { 'no-mobile': active > 2 }) }>
          <div className={ s.layer }/>
          <div className={ s.left }>
            <div className={ s.line }/>
            <h2 className='small bold uppercase m-0 fontSecondary' dangerouslySetInnerHTML={{ __html: data[active].heading }}/>
            <h4 className='small light m-0'>{ data[active].description }</h4>
          </div>
          <div className={ s.right }>
            <ButtonDownload/>
          </div>
        </div>
        <div className={ cn(s.bottomTabs, 'no-desktop no-tablet') }>
          { data.slice(3).map(item =>
            <div key={ item.id } className={ cn(s.item, 'cursor-pointer', { [s.active]: active === item.id }) } onClick={ () => setActive(item.id) }>
              <div className={ s.icon }>
                <img src={ active === item.id ? item.activeIcon : item.icon } alt={ item.title } className='contained'/>
              </div>
              <p className='big light m-0' dangerouslySetInnerHTML={{ __html: item.title }}/>
            </div>
          )}
        </div>
        <div className={ cn(s.bottom, 'no-desktop no-tablet', { 'no-mobile': active < 3 }) }>
          <div className={ s.layer }/>
          <div className={ s.left }>
            <div className={ s.line }/>
            <h2 className='small bold uppercase m-0 fontSecondary' dangerouslySetInnerHTML={{ __html: data[active].heading }}/>
            <h4 className='small light m-0'>{ data[active].description }</h4>
          </div>
          <div className={ s.right }>
            <ButtonDownload/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OurServices
