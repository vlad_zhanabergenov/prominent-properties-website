import React from 'react'

import { Loading } from 'components'

import s from './style.module.sass'

const LoadingScene = () => {
  return (
    <div className={ s.LoadingScene }>
      <Loading size='l'/>
    </div>
  )
}

export default LoadingScene
