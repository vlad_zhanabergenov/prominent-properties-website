import { getPersons } from 'api/contentful'
import { PageConfig } from 'components/Page'
import { GetStaticProps } from 'next'

import PeopleScene from 'scenes/People'

interface Props {
  personList: PersonItem[]
}

export default function People(props: Props) {
  return (
    <PeopleScene { ...props }/>
  )
}

export const getStaticProps: GetStaticProps<Props & { pageConfig: PageConfig }> = async () => {
  const PersonItems = await getPersons()

  return {
    props: {
      pageConfig: {
        title: "People",
        withHeaderStatic: true,
        withHeader: true,
        withFooter: true
      },
      personList: PersonItems
    },
    revalidate: 30
  }
}
