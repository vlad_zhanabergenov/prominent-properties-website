import { PageConfig } from 'components/Page'
import { GetStaticProps } from 'next'

import AboutScene from 'scenes/About'

export default function About() {
  return (
    <AboutScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "About us",
        withHeaderStatic: true,
        withHeader: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
