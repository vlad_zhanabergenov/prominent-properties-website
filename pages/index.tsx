import { PageConfig } from 'components/Page'
import { GetStaticProps } from 'next'
import { getHeroCarouselItems } from 'api/contentful'
import { getFeaturedOfferings } from 'api/backend'

import HomeScene from 'scenes/Home'

interface Props {
  heroCarouselList: HeroCarouselItem[]
  featuredOfferingsList: AnyProperty[]
}

export default function Home(props: Props) {
  return (
    <HomeScene { ...props }/>
  )
}

export const getStaticProps: GetStaticProps<Props & { pageConfig: PageConfig }> = async () => {
  const [
    heroCarouselItems,
    featuredOfferings
  ] = await Promise.all([
    await getHeroCarouselItems(),
    await getFeaturedOfferings()
  ])

  return {
    props: {
      pageConfig: {
        withHeader: true,
        withFooter: true
      },
      heroCarouselList: heroCarouselItems,
      featuredOfferingsList: featuredOfferings
    },
    revalidate: 30
  }
}
