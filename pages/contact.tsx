import { PageConfig } from 'components/Page'
import { GetStaticProps } from 'next'

import ContactScene from 'scenes/Contact'

export default function Contact() {
  return (
    <ContactScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        title: "Contact",
        withHeaderStatic: true,
        withHeader: true,
        withFooter: true
      }
    },
    revalidate: 30
  }
}
