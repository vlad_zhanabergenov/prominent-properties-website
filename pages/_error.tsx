import { PageConfig } from 'components/Page'
import { GetStaticProps } from 'next'

import ErrorScene from 'scenes/Error'

export default function CustomError() {
  return (
    <ErrorScene/>
  )
}

export const getStaticProps: GetStaticProps<{ pageConfig: PageConfig }> = async () => {
  return {
    props: {
      pageConfig: {
        withHeader: true,
        withFooter: true
      }
    }
  }
}
